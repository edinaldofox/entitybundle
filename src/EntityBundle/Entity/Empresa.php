<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Event\KernelEvent;

/**
 * @ORM\Entity
 * @ORM\Table(name="empresa")
 */
class Empresa
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $cpf;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $cnpj;


    protected $contatos;

    /**
     * @ORM\OneToOne(targetEntity="EmpresaEndereco", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="empresa_endereco_id", referencedColumnName="id")
     **/
    protected $endereco;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\EmpresaImagens", mappedBy="empresa", cascade={"persist"}, fetch="LAZY")
     **/
    protected $imagens;

    /**
     * @ORM\Column(type="array", name="menu_disponivel")
     */
    protected $menuDisponivel;

    /**
     * Nome do bundle da empresa no diretorio web
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $bundle;

    /**
     * dominio da empresa
     * @var
     * @ORM\Column(type="string", length=255)
     */
    protected $dominio;

    /**
     * @ORM\Column(type="boolean", name="is_ativo")
     */
    protected $isAtivo;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $telefone;

    /**
     * @ORM\OneToOne(targetEntity="EntityBundle\Entity\ConfiguracaoSistema", mappedBy="empresa", cascade={"persist"}, fetch="LAZY")
     **/
    protected $sistema;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\ConfiguracaoMeiosDePagamento", mappedBy="empresa")
     **/
    protected $meiosDePagamento;

    public function __construct()
    {
        $this->imagens = new ArrayCollection();
        $this->imagens->add(new EmpresaImagens());
        $this->meiosDePagamento = new ArrayCollection();
        $this->endereco = new EmpresaEndereco();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param mixed $cnpj
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContatos()
    {
        return $this->contatos;
    }

    /**
     * @param mixed $contatos
     */
    public function setContatos($contatos)
    {
        $this->contatos = $contatos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param mixed $imagens
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {
            if ($imagem) {
                $imagem->setEmpresa($this);
            }
        }
        $this->imagens = $imagens;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param mixed $isAtivo
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMenuDisponivel()
    {
        return $this->menuDisponivel;
    }

    /**
     * @param mixed $menuDisponivel
     */
    public function setMenuDisponivel($menuDisponivel)
    {
        $this->menuDisponivel = $menuDisponivel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDominio()
    {
        return $this->dominio;
    }

    /**
     * @param mixed $dominio
     */
    public function setDominio($dominio)
    {
        $this->dominio = $dominio;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * @param mixed $bundle
     */
    public function setBundle($bundle)
    {
        $this->bundle = $bundle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    /**
     * @return ConfiguracaoSistema
     */
    public function getSistema()
    {
        return $this->sistema;
    }

    /**
     * @param ConfiguracaoSistema $sistema
     */
    public function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    /**
     * @return ArrayCollection
     */
    public function getMeiosDePagamento()
    {
        return $this->meiosDePagamento;
    }

    /**
     * @param ArrayCollection $meiosDePagamento
     * @return Empresa
     */
    public function setMeiosDePagamento($meiosDePagamento)
    {
        $this->meiosDePagamento = $meiosDePagamento;
        return $this;
    }

}
