<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Imovel;
use SistemaBundle\Model\Carrinho;

/**
 * @ORM\Table(name="carrinho_imovel")
 * @ORM\Entity()
 */
class CarrinhoImovel implements Carrinho
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $quantidade;

    /**
     * @ORM\ManyToMany(targetEntity="EntityBundle\Entity\Imovel")
     * @ORM\JoinTable(name="carrinho_imoveis",
     *      joinColumns={@ORM\JoinColumn(name="carrinho_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="imovel_id", referencedColumnName="id", unique=false)}
     *      )
     **/
    protected $produtos;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", nullable=false)
     */
    protected $empresa;

    public function __construct()
    {
        $this->produtos = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CarrinhoImovel
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     * @return CarrinhoImovel
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProdutos()
    {
        return $this->produtos;
    }

    /**
     * @param ArrayCollection array
     * @return CarrinhoImovel
     */
    public function setProdutos($produtos)
    {
        $this->produtos = $produtos;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return CarrinhoImovel
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

}
