<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="rent_car")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\RentCarRepository")
 */
class RentCar extends Carro
{


    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $preco;

//    /**
//     * @var
//     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Pessoa")
//     * @ORM\JoinColumn(name="pessoa_id", referencedColumnName="id", unique = false)
//     */
//    protected $pessoa;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\RentCarImagens", mappedBy="rentCar", cascade={"persist"}, fetch="LAZY")
     **/
    protected $imagens;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\RentCarPessoa", mappedBy="rentCar")
     */
    protected $rentCarPessoa;

    public function __construct()
    {
        $this->imagens = new ArrayCollection();
        $this->rentCarPessoa = new ArrayCollection();
    }

    /**
     * @return float
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param float $preco
     * @return RentCar
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

    /**
     * @return Pessoa
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param Pessoa $pessoa
     * @return RentCar
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param ArrayCollection $imagens
     * @return RentCar
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {
            if ($imagem->getPath() != null) {
                $imagem->setRentCar($this);
            }
        }

        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRentCarPessoa()
    {
        return $this->rentCarPessoa;
    }

    /**
     * @param ArrayCollection $carroPessoa
     * @return RentCar
     */
    public function setRentCarPessoa($rentCarPessoa)
    {
        $this->rentCarPessoa = $rentCarPessoa;
        return $this;
    }
}
