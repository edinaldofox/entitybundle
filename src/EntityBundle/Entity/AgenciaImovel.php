<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Event\KernelEvent;

/**
 * @ORM\Entity
 * @ORM\Table(name="agencia_imovel")
 */
class AgenciaImovel
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $cpf;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $cnpj;


    protected $contatos;

    /**
     * @ORM\OneToOne(targetEntity="AgenciaImovelEndereco", cascade={"persist"})
     * @ORM\JoinColumn(name="agencia_imovel_endereco_id", referencedColumnName="id")
     **/
    protected $endereco;

    /**
     * @ORM\OneToMany(targetEntity="AgenciaImovelImagem", mappedBy="agenciaImovel", cascade={"persist"})
     **/
    protected $imagens;

    /**
     * @ORM\Column(type="array")
     */
    protected $menuDisponivel;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", nullable=false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAtivo;

    public function __construct()
    {
        $this->imagens = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param mixed $cnpj
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContatos()
    {
        return $this->contatos;
    }

    /**
     * @param mixed $contatos
     */
    public function setContatos($contatos)
    {
        $this->contatos = $contatos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param mixed $imagens
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {

            if ($imagem != null && $imagem->getNome() !== null) {
                $imagem->setAgenciaImovel($this);
            }
        }
        $this->imagens = $imagens;
    }


    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param mixed $isAtivo
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    public function addImagem(AgenciaImovelImagem $imagem)
    {
        $this->imagens->add($imagem);
    }

    /**
     * @return mixed
     */
    public function getMenuDisponivel()
    {
        return $this->menuDisponivel;
    }

    /**
     * @param mixed $menuDisponivel
     */
    public function setMenuDisponivel($menuDisponivel)
    {
        $this->menuDisponivel = $menuDisponivel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

}