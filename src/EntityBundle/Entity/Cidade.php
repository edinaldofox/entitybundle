<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="cidade")
 * @ORM\Entity()
 */
class Cidade
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $locNosub;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $locNo;

    /**
     * @ORM\Column(type="string", length=16)
     */
    protected $cep;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $locInTipoLocalidade;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Uf")
     * @ORM\JoinColumn(name="uf_id", referencedColumnName="uf")
     */
    protected $uf;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocNosub()
    {
        return $this->locNosub;
    }

    /**
     * @param mixed $locNosub
     */
    public function setLocNosub($locNosub)
    {
        $this->locNosub = $locNosub;
    }

    /**
     * @return mixed
     */
    public function getLocNo()
    {
        return $this->locNo;
    }

    /**
     * @param mixed $locNo
     */
    public function setLocNo($locNo)
    {
        $this->locNo = $locNo;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getLocInTipoLocalidade()
    {
        return $this->locInTipoLocalidade;
    }

    /**
     * @param mixed $locInTipoLocalidade
     */
    public function setLocInTipoLocalidade($locInTipoLocalidade)
    {
        $this->locInTipoLocalidade = $locInTipoLocalidade;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

}