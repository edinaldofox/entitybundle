<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Imagem;
use EntityBundle\Entity\ImagemInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="produto_imagens")
 */
class ProdutoImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="Produto", inversedBy="imagens")
     * @ORM\JoinColumn(name="produto_id", referencedColumnName="id")
     **/
    protected $produto;

    /**
     * @return Produto
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param Produto $produto
     * @return ProdutoImagens
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
        return $this;
    }

}