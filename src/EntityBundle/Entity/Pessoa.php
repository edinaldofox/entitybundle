<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use SistemaBundle\Form\Type\EnderecoType;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="pessoa")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\PessoaRepository")
 */
class Pessoa
{

    const PESSOA_FISICA = "PF",
        PESSOA_JURIDICA = "PJ",
        PESSOA_OUTRO = "OUTRO";

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=225, unique=false, nullable=true)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", length=225, unique=false, nullable=true)
     */
    protected $nomeFantasia;

    /**
     * @ORM\Column(type="string", length=225, unique=false, nullable=true)
     */
    protected $sobrenome;

    /**
     * @ORM\Column(type="string", length=60, unique=false)
     */
    protected $email;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PessoaContato", mappedBy="pessoa", cascade={"persist"})
     **/
    protected $contato;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PessoaTipo")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id", unique = false, nullable=true)
     */
    protected $tipo;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PessoaEndereco", mappedBy="pessoa", cascade={"persist"})
     **/
    protected $endereco;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $codigo;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\User", mappedBy="pessoa", cascade={"persist"})
     **/
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PessoaImagens", mappedBy="pessoa", cascade={"persist"}, fetch="LAZY")
     **/
    protected $imagens;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $tipoPessoa;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $cpfCnpj;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $outro;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descricao;

    public function __construct()
    {
        $this->isActive = true;
        $this->imagens = new ArrayCollection();
        $this->endereco = new ArrayCollection();
        $this->contato = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Pessoa
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Pessoa
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomeFantasia()
    {
        return $this->nomeFantasia;
    }

    /**
     * @param string $nomeFantasia
     */
    public function setNomeFantasia($nomeFantasia)
    {
        $this->nomeFantasia = $nomeFantasia;
        return $this;
    }

    /**
     * @return string
     */
    public function getSobrenome()
    {
        return $this->sobrenome;
    }

    /**
     * @param string $sobrenome
     * @return Pessoa
     */
    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = $sobrenome;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param ArrayCollection $imagens
     * @return Pessoa
     */
    public function setImagens($imagens)
    {

        foreach ($imagens as $imagem) {
            if ($imagem->getPath() != null) {
                $imagem->setPessoa($this);
            }
        }

        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return Pessoa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return Contato
     */
    public function getContato()
    {
        return $this->contato;
    }

    /**
     * @param Contato $contato
     * @return Pessoa
     */
    public function setContato($contato)
    {
        $this->contato = $contato;
        return $this;
    }

    /**
     * @return EnderecoType
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param Endereco $endereco
     * @return Pessoa
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return Pessoa
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     * @return Pessoa
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param ArrayCollection $user
     * @return Pessoa
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoPessoa()
    {
        return $this->tipoPessoa;
    }

    /**
     * @param string $tipoPessoa
     * @return Pessoa
     */
    public function setTipoPessoa($tipoPessoa)
    {
        $this->tipoPessoa = $tipoPessoa;
        return $this;
    }

    /**
     * @return string
     */
    public function getCpfCnpj()
    {
        return $this->cpfCnpj;
    }

    /**
     * @param string $cpfCnpj
     * @return Pessoa
     */
    public function setCpfCnpj($cpfCnpj)
    {
        $this->cpfCnpj = $cpfCnpj;
        return $this;
    }

    /**
     * @return string
     */
    public function getOutro()
    {
        return $this->outro;
    }

    /**
     * @param string $outro
     * @return Pessoa
     */
    public function setOutro($outro)
    {
        $this->outro = $outro;
        return $this;
    }

    /**
     * @return PessoaTipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param PessoaTipo $tipo
     * @return Pessoa
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return text
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param text $descricao
     * @return Pessoa
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }
}
