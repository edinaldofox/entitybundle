<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="logradouro")
 * @ORM\Entity()
 */
class Logradouro
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    protected $ufeSg;

    /**
     * @ORM\Column(type="string", length=70)
     */
    protected $logNo;

    /**
     * @ORM\Column(type="string", length=125)
     */
    protected $logNome;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Bairro")
     * @ORM\JoinColumn(name="bairro_id", referencedColumnName="id")
     */
    protected $bairroInicio;

    /**
     * @ORM\Column(type="integer")
     */
    protected $bairroFim;

    /**
     * @ORM\Column(type="string", length=16)
     */
    protected $cep;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $logComplemento;

    /**
     * @ORM\Column(type="string", length=72)
     */
    protected $logTipoLogradouro;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $logStatusTipoLog;

    /**
     * @ORM\Column(type="string", length=70)
     */
    protected $logNoSemAcento;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUfeSg()
    {
        return $this->ufeSg;
    }

    /**
     * @param mixed $ufeSg
     */
    public function setUfeSg($ufeSg)
    {
        $this->ufeSg = $ufeSg;
    }

    /**
     * @return mixed
     */
    public function getLogNo()
    {
        return $this->logNo;
    }

    /**
     * @param mixed $logNo
     */
    public function setLogNo($logNo)
    {
        $this->logNo = $logNo;
    }

    /**
     * @return mixed
     */
    public function getLogNome()
    {
        return $this->logNome;
    }

    /**
     * @param mixed $logNome
     */
    public function setLogNome($logNome)
    {
        $this->logNome = $logNome;
    }

    /**
     * @return mixed
     */
    public function getBairroInicio()
    {
        return $this->bairroInicio;
    }

    /**
     * @param mixed $bairroInicio
     */
    public function setBairroInicio($bairroInicio)
    {
        $this->bairroInicio = $bairroInicio;
    }

    /**
     * @return mixed
     */
    public function getBairroFim()
    {
        return $this->bairroFim;
    }

    /**
     * @param mixed $bairroFim
     */
    public function setBairroFim($bairroFim)
    {
        $this->bairroFim = $bairroFim;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getLogComplemento()
    {
        return $this->logComplemento;
    }

    /**
     * @param mixed $logComplemento
     */
    public function setLogComplemento($logComplemento)
    {
        $this->logComplemento = $logComplemento;
    }

    /**
     * @return mixed
     */
    public function getLogTipoLogradouro()
    {
        return $this->logTipoLogradouro;
    }

    /**
     * @param mixed $logTipoLogradouro
     */
    public function setLogTipoLogradouro($logTipoLogradouro)
    {
        $this->logTipoLogradouro = $logTipoLogradouro;
    }

    /**
     * @return mixed
     */
    public function getLogStatusTipoLog()
    {
        return $this->logStatusTipoLog;
    }

    /**
     * @param mixed $logStatusTipoLog
     */
    public function setLogStatusTipoLog($logStatusTipoLog)
    {
        $this->logStatusTipoLog = $logStatusTipoLog;
    }

    /**
     * @return mixed
     */
    public function getLogNoSemAcento()
    {
        return $this->logNoSemAcento;
    }

    /**
     * @param mixed $logNoSemAcento
     */
    public function setLogNoSemAcento($logNoSemAcento)
    {
        $this->logNoSemAcento = $logNoSemAcento;
    }

}