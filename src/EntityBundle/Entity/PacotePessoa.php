<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pacote_pessoa")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\PacotePessoaRepository")
 */
class PacotePessoa
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Pacote
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PacotePasseios", inversedBy="pacotePessoas")
     * @ORM\JoinColumn(name="pacote_id", referencedColumnName="id")
     */
    private $pacote;

    /**
     * @var Pessoa
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Pessoa")
     * @ORM\JoinColumn(name="pessoa_id", referencedColumnName="id")
     */
    private $pessoa;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PacotePessoa
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Pacote
     */
    public function getPacote()
    {
        return $this->pacote;
    }

    /**
     * @param Pacote $pacote
     * @return PacotePessoa
     */
    public function setPacote($pacote)
    {
        $this->pacote = $pacote;
        return $this;
    }

    /**
     * @return Pessoa
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param Pessoa $pessoa
     * @return PacotePessoa
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
        return $this;
    }

}