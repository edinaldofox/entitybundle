<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="locadora_rent_a_car")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\LocadoraRentACarRepository")
 */
class LocadoraRentACar
{

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $nome;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $cpfCnpj;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $telefone;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $email;

    /**
     * @var LocadoraRentACarEndereco
     * @ORM\OneToOne(targetEntity="EntityBundle\Entity\LocadoraRentACarEndereco", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="locadora_endereco_id", referencedColumnName="id")
     **/
    protected $endereco;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\LocadoraRentACarImagens", mappedBy="locadora", cascade={"persist"}, fetch="LAZY")
     **/
    protected $imagens;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\LocadoraVeiculo", mappedBy="locadora")
     */
    protected $locadoraVeiculo;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\LocadoraPessoa", mappedBy="locadora")
     */
    protected $locadoraPessoa;

    public function __construct()
    {
        $this->imagens = new ArrayCollection();
        $this->locadoraVeiculo = new ArrayCollection();
        $this->locadoraPessoa = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return LocadoraRentACar
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return LocadoraRentACar
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getCpfCnpj()
    {
        return $this->cpfCnpj;
    }

    /**
     * @param string $cpfCnpj
     * @return LocadoraRentACar
     */
    public function setCpfCnpj($cpfCnpj)
    {
        $this->cpfCnpj = $cpfCnpj;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param string $telefone
     * @return LocadoraRentACar
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return LocadoraRentACar
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return LocadoraRentACarEndereco
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param LocadoraRentACarEndereco $endereco
     * @return LocadoraRentACar
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param ArrayCollection $imagens
     * @return LocadoraRentACar
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {
            if ($imagem) {
                $imagem->setLocadora($this);
            }
        }
        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return LocadoraRentACar
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLocadoraVeiculo()
    {
        return $this->locadoraVeiculo;
    }

    /**
     * @param ArrayCollection $locadoraVeiculo
     * @return LocadoraRentACar
     */
    public function setLocadoraVeiculo($locadoraVeiculo)
    {
        $this->locadoraVeiculo = $locadoraVeiculo;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLocadoraPessoa()
    {
        return $this->locadoraPessoa;
    }

    /**
     * @param ArrayCollection $locadoraPessoa
     * @return LocadoraRentACar
     */
    public function setLocadoraPessoa($locadoraPessoa)
    {
        $this->locadoraPessoa = $locadoraPessoa;
        return $this;
    }

}
