<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pacote_intinerarios")
 * @ORM\Entity()
 */
class PacoteIntinerarios
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $saida;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $diaSaida;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    protected $horaSaida;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $chegada;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $diaChegada;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    protected $horaChegada;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PacotePasseios", inversedBy="intinerarios")
     * @ORM\JoinColumn(name="pacote_id", referencedColumnName="id", unique = false)
     */
    protected $pacote;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Uf")
     * @ORM\JoinColumn(name="uf_id", referencedColumnName="uf")
     */
    protected $estado;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return PasseioHorarioIndisponivel
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSaida()
    {
        return $this->saida;
    }

    /**
     * @param string $saida
     * @return PacoteIntinerarios
     */
    public function setSaida($saida)
    {
        $this->saida = $saida;
        return $this;
    }

    /**
     * @return date
     */
    public function getDiaSaida()
    {
        return $this->diaSaida;
    }

    /**
     * @param date $diaSaida
     * @return PacoteIntinerarios
     */
    public function setDiaSaida($diaSaida)
    {
        $this->diaSaida = $diaSaida;
        return $this;
    }

    /**
     * @return time
     */
    public function getHoraSaida()
    {
        return $this->horaSaida;
    }

    /**
     * @param time $horaSaida
     * @return PacoteIntinerarios
     */
    public function setHoraSaida($horaSaida)
    {
        $this->horaSaida = $horaSaida;
        return $this;
    }

    /**
     * @return string
     */
    public function getChegada()
    {
        return $this->chegada;
    }

    /**
     * @param string $chegada
     * @return PacoteIntinerarios
     */
    public function setChegada($chegada)
    {
        $this->chegada = $chegada;
        return $this;
    }

    /**
     * @return date
     */
    public function getDiaChegada()
    {
        return $this->diaChegada;
    }

    /**
     * @param date $diaChegada
     * @return PacoteIntinerarios
     */
    public function setDiachegada($diaChegada)
    {
        $this->diaChegada = $diaChegada;
        return $this;
    }

    /**
     * @return time
     */
    public function getHoraChegada()
    {
        return $this->horaChegada;
    }

    /**
     * @param time $horaChegada
     * @return PacoteIntinerarios
     */
    public function setHoraChegada($horaChegada)
    {
        $this->horaChegada = $horaChegada;
        return $this;
    }

    /**
     * @return PacotePasseios
     */
    public function getPacote()
    {
        return $this->pacote;
    }

    /**
     * @param PacotePasseios $pacote
     * @return PacoteIntinerarios
     */
    public function setPacote($pacote)
    {
        $this->pacote = $pacote;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

}
