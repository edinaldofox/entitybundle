<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Imagem;

/**
 * @ORM\Entity
 * @ORM\Table(name="imovel_imagens")
 */
class ImovelImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="Imovel", inversedBy="imagens")
     * @ORM\JoinColumn(name="imovel_id", referencedColumnName="id", nullable=false)
     **/
    protected $imovel;

    /**
     * @return Imovel
     */
    public function getImovel()
    {
        return $this->imovel;
    }

    /**
     * @param Imovel $imovel
     * @return ImovelImagens
     */
    public function setImovel($imovel)
    {
        $this->imovel = $imovel;
        return $this;
    }

}
