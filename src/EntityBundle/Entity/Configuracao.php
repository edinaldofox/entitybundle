<?php

namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Empresa;
use Symfony\Component\HttpKernel\Event\KernelEvent;

/*
 * @ORM\Entity
 * @ORM\Table(name="site_configuracao")
 */
class Configuracao extends Empresa
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $adWords;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAdWords()
    {
        return $this->adWords;
    }

    /**
     * @param mixed $adWords
     */
    public function setAdWords($adWords)
    {
        $this->adWords = $adWords;
    }



}