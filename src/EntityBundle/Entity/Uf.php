<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="uf")
 * @ORM\Entity()
 */
class Uf
{



    /**
     * @ORM\Column(type="string")
     * @ORM\Id
     */
    protected $uf;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $estado;

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}