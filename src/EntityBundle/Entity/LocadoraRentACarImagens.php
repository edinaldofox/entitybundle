<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="locadora_rent_a_car_imagens")
 */
class LocadoraRentACarImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\LocadoraRentACar", inversedBy="imagens")
     * @ORM\JoinColumn(name="locadora_id", referencedColumnName="id")
     **/
    protected $locadora;

    /**
     * @return LocadoraRentACar
     */
    public function getLocadora()
    {
        return $this->locadora;
    }

    /**
     * @param LocadoraRentACar $empresa
     * @return LocadoraRentACarImagens
     */
    public function setLocadora($locadora)
    {
        $this->locadora = $locadora;
        return $this;
    }
}