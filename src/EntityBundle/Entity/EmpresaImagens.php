<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Imagem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="empresa_imagens")
 */
class EmpresaImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="Empresa", inversedBy="imagens")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     **/
    protected $empresa;

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return EmpresaImagens
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }
}