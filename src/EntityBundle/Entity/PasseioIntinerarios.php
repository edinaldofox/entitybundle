<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="passeio_intinerarios")
 * @ORM\Entity()
 */
class PasseioIntinerarios
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $saida;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $diaSaida;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    protected $horaSaida;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $chegada;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $diaChegada;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    protected $horaChegada;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Passeio", inversedBy="intinerarios")
     * @ORM\JoinColumn(name="passeio_id", referencedColumnName="id", unique = false)
     */
    protected $passeio;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return PasseioHorarioIndisponivel
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSaida()
    {
        return $this->saida;
    }

    /**
     * @param string $saida
     * @return PasseioIntinerarios
     */
    public function setSaida($saida)
    {
        $this->saida = $saida;
        return $this;
    }

    /**
     * @return date
     */
    public function getDiaSaida()
    {
        return $this->diaSaida;
    }

    /**
     * @param date $diaSaida
     * @return PasseioIntinerarios
     */
    public function setDiaSaida($diaSaida)
    {
        $this->diaSaida = $diaSaida;
        return $this;
    }

    /**
     * @return time
     */
    public function getHoraSaida()
    {
        return $this->horaSaida;
    }

    /**
     * @param time $horaSaida
     * @return PasseioIntinerarios
     */
    public function setHoraSaida($horaSaida)
    {
        $this->horaSaida = $horaSaida;
        return $this;
    }

    /**
     * @return string
     */
    public function getChegada()
    {
        return $this->chegada;
    }

    /**
     * @param string $chegada
     * @return PasseioIntinerarios
     */
    public function setChegada($chegada)
    {
        $this->chegada = $chegada;
        return $this;
    }

    /**
     * @return date
     */
    public function getDiaChegada()
    {
        return $this->diaChegada;
    }

    /**
     * @param date $diaChegada
     * @return PasseioIntinerarios
     */
    public function setDiachegada($diaChegada)
    {
        $this->diaChegada = $diaChegada;
        return $this;
    }

    /**
     * @return time
     */
    public function getHoraChegada()
    {
        return $this->horaChegada;
    }

    /**
     * @param time $horaChegada
     * @return PasseioIntinerarios
     */
    public function setHoraChegada($horaChegada)
    {
        $this->horaChegada = $horaChegada;
        return $this;
    }

    /**
     * @return Passeio
     */
    public function getPasseio()
    {
        return $this->passeio;
    }

    /**
     * @param Passeio $passeio
     * @return PasseioIntinerarios
     */
    public function setPasseio($passeio)
    {
        $this->passeio = $passeio;
        return $this;
    }

}
