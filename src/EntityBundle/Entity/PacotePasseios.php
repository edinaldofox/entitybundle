<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pacote_passeios")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\PacotePasseioRepository")
 */
class PacotePasseios
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $titulo;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $nome;

    /**
     * @ORM\Column(type="text")
     */
    protected $descricao;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PacotePasseiosCategoria")
     * @ORM\JoinColumn(name="pacote_passeio_categoria_id", referencedColumnName="id", unique = false, nullable=true)
     **/
    protected $categoria;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PacotePasseiosImagens", mappedBy="pacotePasseios", cascade={"persist"}, fetch="LAZY")
     **/
    protected $imagens;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $ativo;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\Passeio", mappedBy="pacotePasseios", cascade={"persist"}, fetch="LAZY")
     **/
    protected $passeios;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PacoteIntinerarios", mappedBy="pacote", cascade={"persist"})
     */
    protected $intinerarios;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PacotePessoa", mappedBy="pacote")
     */
    protected $pacotePessoas;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Uf")
     * @ORM\JoinColumn(name="uf_id", referencedColumnName="uf")
     */
    protected $estado;

    public function __construct()
    {
        $this->imagens = new ArrayCollection();
        $this->passeios = new ArrayCollection();
        $this->intinerarios = new ArrayCollection();
        $this->pacotePessoas = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return PacotePasseios
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return PacotePasseios
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return PacotePasseios
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     * @return PacotePasseios
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return PacotePasseiosCategoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param PacotePasseiosCategoria $categoria
     * @return PacotePasseios
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param ArrayCollection $imagens
     * @return PacotePasseios
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {
            if ($imagem->getPath() != null) {
                $imagem->setPacotePasseios($this);
            }
        }

        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return PacotePasseios
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param boolean $ativo
     * @return PacotePasseios
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPasseios()
    {
        return $this->passeios;
    }

    /**
     * @param ArrayCollection $passeios
     * @return PacotePasseios
     */
    public function setPasseios($passeios)
    {
        $this->passeios = $passeios;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getIntinerarios()
    {
        return $this->intinerarios;
    }

    /**
     * @param ArrayCollection $intinerarios
     */
    public function setIntinerarios($intinerarios)
    {
        $this->intinerarios = $intinerarios;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPacotePessoas()
    {
        return $this->pacotePessoas;
    }

    /**
     * @param ArrayCollection $pacotePessoas
     * @return PacotePasseios
     */
    public function setPacotePessoas($pacotePessoas)
    {
        $this->pacotePessoas = $pacotePessoas;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }
}
