<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="passeio_pessoa")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\PasseioPessoaRepository")
 */
class PasseioPessoa
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Passeio
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Passeio", inversedBy="passeioPessoas")
     * @ORM\JoinColumn(name="passeio_id", referencedColumnName="id")
     */
    private $passeio;

    /**
     * @var Pessoa
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Pessoa")
     * @ORM\JoinColumn(name="pessoa_id", referencedColumnName="id")
     */
    private $pessoa;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PasseioPessoa
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Passeio
     */
    public function getPasseio()
    {
        return $this->passeio;
    }

    /**
     * @param Passeio $passeio
     * @return PasseioPessoa
     */
    public function setPasseio($passeio)
    {
        $this->passeio = $passeio;
        return $this;
    }

    /**
     * @return Pessoa
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param Pessoa $pessoa
     * @return PasseioPessoa
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
        return $this;
    }

}