<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="configuracao_sistema")
 * @ORM\Entity()
 */
class ConfiguracaoSistema
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="meta_tag_description", length=90, nullable=true)
     */
    protected $metaTagDescription;

    /**
     * @ORM\Column(type="string", name="meta_tag_keywords", length=150, nullable=true)
     */
    protected $metaTagKeywords;

    /**
     * @ORM\Column(type="text", name="google_analytics", nullable=true)
     */
    protected $googleAnalytics;

    /**
     * @ORM\Column(type="string", name="titulo_site", length=150, nullable=true)
     */
    protected $tituloSite;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="Empresa", inversedBy="sistema")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return ConfiguracaoSistema
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTagDescription()
    {
        return $this->metaTagDescription;
    }

    /**
     * @param string $metaTagDescription
     * @return ConfiguracaoSistema
     */
    public function setMetaTagDescription($metaTagDescription)
    {
        $this->metaTagDescription = $metaTagDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTagKeywords()
    {
        return $this->metaTagKeywords;
    }

    /**
     * @param string $metaTagKeywords
     * @return ConfiguracaoSistema
     */
    public function setMetaTagKeywords($metaTagKeywords)
    {
        $this->metaTagKeywords = $metaTagKeywords;
        return $this;
    }

    /**
     * @return text
     */
    public function getGoogleAnalytics()
    {
        return $this->googleAnalytics;
    }

    /**
     * @param text $googleAnalytics
     * @return ConfiguracaoSistema
     */
    public function setGoogleAnalytics($googleAnalytics)
    {
        $this->googleAnalytics = $googleAnalytics;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return ConfiguracaoSistema
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return string
     */
    public function getTituloSite()
    {
        return $this->tituloSite;
    }

    /**
     * @param string $tituloSite
     * @return ConfiguracaoSistema
     */
    public function setTituloSite($tituloSite)
    {
        $this->tituloSite = $tituloSite;
        return $this;
    }

}
