<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="bairro")
 * @ORM\Entity()
 */
class Bairro
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    protected $uf;

    /**
     * @ORM\Column(type="string", length=72)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", length=36)
     */
    protected $nomeAbreviado;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Cidade")
     * @ORM\JoinColumn(name="cidade_id", referencedColumnName="id")
     */
    protected $cidade;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getNomeAbreviado()
    {
        return $this->nomeAbreviado;
    }

    /**
     * @param mixed $nomeAbreviado
     */
    public function setNomeAbreviado($nomeAbreviado)
    {
        $this->nomeAbreviado = $nomeAbreviado;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

}
