<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="passeio_horario_indisponivel")
 * @ORM\Entity()
 */
class PasseioHorarioIndisponivel
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $data;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Passeio", inversedBy="horariosIndisponiveis")
     * @ORM\JoinColumn(name="passeio_id", referencedColumnName="id", unique = false)
     */
    protected $passeio;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return PasseioHorarioIndisponivel
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return datetime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param datetime $data
     * @return PasseioHorarioIndisponivel
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return Passeio
     */
    public function getPasseio()
    {
        return $this->passeio;
    }

    /**
     * @param Passeio $passeio
     * @return PasseioHorarioIndisponivel
     */
    public function setPasseio($passeio)
    {
        $this->passeio = $passeio;
        return $this;
    }
}
