<?php

namespace EntityBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Imagem;

/**
 * @ORM\Entity
 * @ORM\Table(name="pessoa_imagem")
 */
class PessoaImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Pessoa", inversedBy="imagens")
     * @ORM\JoinColumn(name="pessoa_imagem_id", referencedColumnName="id")
     **/
    protected $pessoa;

    /**
     * @return Pessoa
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param Pessoa $pessoa
     * @return PessoasImagens
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
        return $this;
    }
}