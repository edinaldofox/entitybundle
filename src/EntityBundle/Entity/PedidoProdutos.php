<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pedido_produtos")
 * @ORM\Entity()
 */
class PedidoProdutos
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $nome;

    /**
     * @ORM\Column(type="float")
     */
    protected $preco;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dataInicial;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dataFinal;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantidade;

    /**
     * @ORM\Column(type="text")
     */
    protected $descricao;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Pedido", inversedBy="produtos")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     **/
    protected $pedido;

    /**
     * @ORM\Column(type="integer")
     */
    protected $produto_id;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PedidoProdutoTipo")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     **/
    protected $tipo;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PedidoProdutos
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return PedidoProdutos
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return float
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param float $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     * @return PedidoProdutos
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     * @return PedidoProdutos
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * @param Pedido $pedido
     * @return PedidoProdutos
     */
    public function setPedido($pedido)
    {
        $this->pedido = $pedido;
        return $this;
    }

    /**
     * @return int
     */
    public function getProdutoId()
    {
        return $this->produto_id;
    }

    /**
     * @param int $produto_id
     * @return PedidoProdutos
     */
    public function setProdutoId($produto_id)
    {
        $this->produto_id = $produto_id;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataInicial()
    {
        return $this->dataInicial;
    }

    /**
     * @param DateTime $dataInicial
     * @return PedidoProdutos
     */
    public function setDataInicial($dataInicial)
    {
        $this->dataInicial = $dataInicial;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataFinal()
    {
        return $this->dataFinal;
    }

    /**
     * @param DateTime $dataFinal
     * @return PedidoProdutos
     */
    public function setDataFinal($dataFinal)
    {
        $this->dataFinal = $dataFinal;
        return $this;
    }

    /**
     * @return PedidoProdutoTipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param PedidoProdutoTipo $tipo
     * @return PedidoProdutos
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

}
