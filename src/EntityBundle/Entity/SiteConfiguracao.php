<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Empresa;
use Symfony\Component\HttpKernel\Event\KernelEvent;

/**
 * @ORM\Entity
 * @ORM\Table(name="site_configuracao")
 */
class SiteConfiguracao
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=250, unique = false)
     *
     */
    protected $keywords;

    /**
     * @ORM\Column(type="string", length=250, unique = false)
     *
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=250, unique = false)
     *
     */
    protected $autor;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $ativo;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dataCadastro;

    public function __construct() {
        $this->ativo = true;
        $this->dataCadastro = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SiteConfiguracao
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     * @return SiteConfiguracao
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return SiteConfiguracao
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * @param mixed $autor
     * @return SiteConfiguracao
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param $ativo
     * @return SiteConfiguracao
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param mixed $dataCadastro
     * @return SiteConfiguracao
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     * @return SiteConfiguracao
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }
}