<?php

namespace EntityBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rent_car_imagem")
 */
class RentCarImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\RentCar", inversedBy="imagens")
     * @ORM\JoinColumn(name="rent_car_imagem_id", referencedColumnName="id")
     **/
    protected $rentCar;

    /**
     * @return RentCar
     */
    public function getRentCar()
    {
        return $this->rentCar;
    }

    /**
     * @param RentCar $rentCar
     * @return RentCarImagens
     */
    public function setRentCar($rentCar)
    {
        $this->rentCar = $rentCar;
        return $this;
    }
}