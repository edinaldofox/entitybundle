<?php

namespace EntityBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="imovel_pedido_historicos")
 */
class ImovelPedidoHistorico
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $valorDesconto;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $valor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dataInicio;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dataFinal;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $dataCadastro;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\ImovelPedidos", inversedBy="historicos")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id", unique = false)
     **/
    protected $pedido;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Imovel")
     * @ORM\JoinColumn(name="imovel_id", referencedColumnName="id", unique = false)
     **/
    protected $imovel;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\User")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", unique = false)
     */
    protected $usuario;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAtivo;

    public function __construct() {

    }

    /**
     * @return insteger
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return PedidoHistorico
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorDesconto()
    {
        return $this->valorDesconto;
    }

    /**
     * @param float $valorDesconto
     * @return PedidoHistorico
     */
    public function setValorDesconto($valorDesconto)
    {
        $this->valorDesconto = $valorDesconto;
        return $this;
    }

    /**
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param float $valor
     * @return PedidoHistorico
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return date
     */
    public function getDataInicio()
    {
        return $this->dataInicio;
    }

    /**
     * @param date $dataInicio
     * @return PedidoHistorico
     */
    public function setDataInicio($dataInicio)
    {
        $this->dataInicio = $dataInicio;
        return $this;
    }

    /**
     * @return date
     */
    public function getDataFinal()
    {
        return $this->dataFinal;
    }

    /**
     * @param date $dataFinal
     * @return PedidoHistorico
     */
    public function setDataFinal($dataFinal)
    {
        $this->dataFinal = $dataFinal;
        return $this;
    }

    /**
     * @return date
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param date $dataCadastro
     * @return PedidoHistorico
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * @param mixed $pedido
     * @return PedidoHistorico
     */
    public function setPedido($pedido)
    {
        $this->pedido = $pedido;
        return $this;
    }

    /**
     * @return User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     * @return PedidoHistorico
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return PedidoHistorico
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param mixed $isAtivo
     * @return PedidoHistorico
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImovel()
    {
        return $this->imovel;
    }

    /**
     * @param mixed $imovel
     */
    public function setImovel($imovel)
    {
        $this->imovel = $imovel;
    }

}