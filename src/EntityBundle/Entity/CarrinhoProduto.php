<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Produto;

/**
 * @ORM\Table(name="carrinho_produto")
 * @ORM\Entity()
 */
class CarrinhoProduto implements Carrinho
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $quantidade;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Produto")
     * @ORM\JoinColumn(name="produto_id", referencedColumnName="id")
     **/
    protected $produto;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", nullable=false)
     */
    protected $empresa;

    public function __construct() {
        $this->produtos = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CarrinhoProduto
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     * @return CarrinhoProduto
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProdutos()
    {
        return $this->produto;
    }

    /**
     * @param ArrayCollection array
     * @return CarrinhoProduto
     */
    public function setProdutos($produto)
    {
        $this->produto = $produto;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return CarrinhoProduto
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

}
