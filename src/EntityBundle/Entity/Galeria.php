<?php

namespace EntityBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="galerias")
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\GaleriaRepository")
 */
class Galeria 
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=455)
     */
    protected $nome;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descricao;

    /**
     * @ORM\ManyToOne(targetEntity="GaleriaCategoria")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", unique = false)
     **/
    protected $categoria;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAtivo;

    /**
     * @ORM\OneToMany(targetEntity="GaleriaImagens", mappedBy="galeria", cascade={"persist"})
     **/
    protected $imagens;

    public function __construct() {
        $this->imagens = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param $nome
     * @return $this
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param $descricao
     * @return $this
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param $isAtivo
     * @return $this
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param $imagens
     * @return Galeria
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {

            if ($imagem->getPath() != null) {
                $imagem->setGaleria($this);
            }
        }

        $this->imagens = $imagens;
        return $this;

        return $this;
    }

    public function addImagens(GaleriaImagens $imagens)
    {
        $this->imagens->add($imagens);
        return $this;
    }

    public function validaImagens()
    {
        $listaDeImagem = new ArrayCollection();
        foreach($this->imagens as $imagem) {
            if($imagem) {
                $listaDeImagem->add($imagem);
            }
        }
        $this->imagens = $listaDeImagem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

}