<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="agencia_imovel_imagem")
 */
class AgenciaImovelImagem
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $nome;

    /**
     * @ORM\ManyToOne(targetEntity="AgenciaImovel", inversedBy="imagens")
     * @ORM\JoinColumn(name="agencia_imovel_id", referencedColumnName="id")
     **/
    protected $agenciaImovel;

    protected $file;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getAgenciaImovel()
    {
        return $this->agenciaImovel;
    }

    /**
     * @param mixed $agenciaImovel
     */
    public function setAgenciaImovel($agenciaImovel)
    {
        $this->agenciaImovel = $agenciaImovel;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        if (null !== $file) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $imagem = $filename.'.'.$file->guessExtension();
        }

        $file->move(__DIR__.'/../../../web/bundles/sistema/imagens/', $imagem);

        $this->nome = $imagem;
    }

}