<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pedido_status")
 * @ORM\Entity()
 */
class PedidoStatus
{

    const AGUARDANDO_PAGAMENTO = 1,
        PAGO = 2;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\StatusDePagamento")
     * @ORM\JoinColumn(name="status_de_pagamento_id", referencedColumnName="id")
     **/
    protected $status;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $descricao;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $data;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Pedido", inversedBy="status")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     **/
    protected $pedido;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PedidoStatus
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return PedidoStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     * @return PedidoStatus
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param datetime $data
     * @return PedidoStatus
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * @param Pedido $pedido
     * @return PedidoStatus
     */
    public function setPedido($pedido)
    {
        $this->pedido = $pedido;
        return $this;
    }

}
