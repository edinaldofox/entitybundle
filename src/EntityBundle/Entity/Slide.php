<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpKernel\Event\KernelEvent;

/**
 * @ORM\Entity
 * @ORM\Table(name="site_slide")
 */
class Slide
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nome;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\SlideImagens", mappedBy="slide", cascade={"persist"})
     **/
    protected $imagens;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean", name="is_ativo", options={"default" = true})
     */
    protected $isAtivo;

    public function __construct()
    {
        $this->imagens = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param mixed $imagens
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {
            if ($imagem) {
                $imagem->setSlide($this);
            }
        }
        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param mixed $isAtivo
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    public function addImagem(SlideImagens $imagem)
    {
        $this->imagens->add($imagem);
        return $this;
    }

    public function validaImagens()
    {
        $listaDeImagem = new ArrayCollection();
        foreach($this->imagens as $imagem) {
            if($imagem) {
                $listaDeImagem->add($imagem);
            }
        }
        $this->imagens = $listaDeImagem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }
}