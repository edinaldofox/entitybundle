<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="produtos")
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\ProdutoRepository")
 *
 */
class Produto implements ProdutoInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $titulo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $star;

    /**
     * @ORM\Column(type="float", scale=2)
     */
    protected $preco;

    /**
     * @ORM\Column(type="float", scale=2)
     */
    protected $desconto;

    /**
     * @ORM\Column(type="text", nullable=true, columnDefinition="TEXT")
     */
    protected $descricao;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $quantidade;

    /**
     * @ORM\ManyToOne(targetEntity="ProdutoCategoria")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", unique = false)
     **/
    protected $categoria;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dataCadastro;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isPromocional;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\ProdutoImagens", mappedBy="produto", cascade={"persist"})
     **/
    protected $imagens;

    /**
     * @ORM\OneToOne(targetEntity="TipoDeProduto", cascade={"persist"})
     * @ORM\JoinColumn(name="tipo_produto_id", referencedColumnName="id")
     **/
    protected $tipoDeProduto;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", nullable=false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    protected $isDestaque;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAtivo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isVisivel;

    public function __construct() {
        $this->dataCadastro = new \DateTime();
        $this->imagens = new ArrayCollection();
        $this->isPromocional = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param mixed $desconto
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @return Produto
     * @param mixed $categorias
     */
    public function setCategoria( $categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param mixed $dataCadastro
     * @return $this
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPromocional()
    {
        return $this->isPromocional;
    }

    /**
     * @param boolean $isPromocional
     * @return $this
     */
    public function setIsPromocional($isPromocional)
    {
        $this->isPromocional = $isPromocional;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param mixed $imagens
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {
            if ($imagem) {
                $imagem->setProduto($this);
            }
        }
        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param mixed $isAtivo
     * @return $this
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

//    public function validaImagens()
//    {
//
//        $listaDeImagem = new ArrayCollection();
//        foreach($this->imagens as $imagem) {
//            if($imagem->getNome() !== null) {
//                $listaDeImagem->add($imagem);
//            }
//        }
//
//        $this->imagens = $listaDeImagem;
//        return $this;
//    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return float
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param $preco
     * @return Produto
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

    /**
     * @return TipoProduto
     */
    public function getTipoDeProduto()
    {
        return $this->tipoDeProduto;
    }

    /**
     * @param mixed $tipoDeProduto
     * @return Produto
     */
    public function setTipoDeProduto($tipoDeProduto)
    {
        $this->tipoDeProduto = $tipoDeProduto;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsVisivel()
    {
        return $this->isVisivel;
    }

    /**
     * @param mixed $isVisivel
     */
    public function setIsVisivel($isVisivel)
    {
        $this->isVisivel = $isVisivel;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getStar()
    {
        return $this->star;
    }

    /**
     * @param mixed $star
     */
    public function setStar($star)
    {
        $this->star = $star;
    }

    /**
     * @return mixed
     */
    public function getIsDestaque()
    {
        return $this->isDestaque;
    }

    /**
     * @param mixed $isDestaque
     */
    public function setIsDestaque($isDestaque)
    {
        $this->isDestaque = $isDestaque;
    }

    /**
     * @return mixed
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param mixed $quantidade
     * @return Produto
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

}
