<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="locadora_rent_a_car_pessoa")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\LocadoraPessoaRepository")
 */
class LocadoraPessoa
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var LocadoraRentACar
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\LocadoraRentACar", inversedBy="passeioPessoas")
     * @ORM\JoinColumn(name="locadora_id", referencedColumnName="id")
     */
    private $locadora;

    /**
     * @var RentCar
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Pessoa")
     * @ORM\JoinColumn(name="pessoa_id", referencedColumnName="id")
     */
    private $pessoa;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return LocadoraRentACar
     */
    public function getLocadora()
    {
        return $this->locadora;
    }

    /**
     * @param LocadoraRentACar $locadora
     * @return LocadoraPessoa
     */
    public function setLocadora($locadora)
    {
        $this->locadora = $locadora;
        return $this;
    }

    /**
     * @return RentCar
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param RentCar $pessoa
     * @return LocadoraPessoa
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
        return $this;
    }

}