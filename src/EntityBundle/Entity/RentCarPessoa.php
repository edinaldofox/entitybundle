<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="rent_car_pessoa")
 * @ORM\Entity()
 */
class RentCarPessoa
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\RentCar", inversedBy="carroPessoa")
     * @ORM\JoinColumn(name="rent_car_id", referencedColumnName="id")
     */
    private $rentCar;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumn(name="pessoa_id", referencedColumnName="id")
     */
    private $pessoa;

    /**
     * @var Empresa
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @return ArrayCollection
     */
    public function getRentCar()
    {
        return $this->rentCar;
    }

    /**
     * @param ArrayCollection $rentCar
     */
    public function setRentCar($rentCar)
    {
        $this->rentCar = $rentCar;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param ArrayCollection $pessoa
     * @return CarroPessoa
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return RentCarPessoa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

}
