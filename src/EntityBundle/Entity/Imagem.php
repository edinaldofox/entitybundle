<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class Imagem implements ImagemInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $path;

    /**
     * @ORM\Column(name="is_ativo", type="boolean", options={"default" = true})
     */
    protected $isAtivo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ordem;

    function __construct()
    {
        $this->isAtivo = false;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return Imagem
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Imagem
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param File $path
     * @return Imagem
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @return boolean
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param boolean $isAtivo
     * @return Imagem
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    /**
     * @return integer
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * @param integer $ordem
     * @return Imagem
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

}
