<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="passeio")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\PasseioRepository")
 */
class Passeio
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $titulo;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $nome;

    /**
     * @ORM\Column(type="text")
     */
    protected $descricao;

    /**
     * @ORM\Column(type="float")
     */
    protected $preco;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $desconto;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PacotePasseiosCategoria")
     * @ORM\JoinColumn(name="pacote_passeio_categoria_id", referencedColumnName="id", unique = false, nullable=true)
     **/
    protected $categoria;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PasseioImagens", mappedBy="passeio", cascade={"persist"}, fetch="LAZY")
     **/
    protected $imagens;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Passeio")
     * @ORM\JoinColumn(name="passeio_referencia", referencedColumnName="id", unique = false, nullable=true)
     **/
    protected $passeioReferencia;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PacotePasseios")
     * @ORM\JoinColumn(name="pacote_passeios_id", referencedColumnName="id", unique = false)
     */
    protected $pacotePasseios;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\User")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", unique = false)
     */
    protected $usuario;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

     /**
     * @var
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PasseioHorarioIndisponivel", mappedBy="passeio")
     */
    protected $horariosIndisponiveis;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PasseioIntinerarios", mappedBy="passeio", cascade={"persist"})
     */
    protected $intinerarios;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $ativo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $visivel;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Uf")
     * @ORM\JoinColumn(name="uf_id", referencedColumnName="uf")
     */
    protected $estado;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PasseioPessoa", mappedBy="passeio")
     */
    protected $passeioPessoas;

    public function __construct()
    {
        $this->imagens = new ArrayCollection();
        $this->horariosIndisponiveis = new ArrayCollection();
        $this->intinerarios = new ArrayCollection();
        $this->passeioPessoas = new ArrayCollection();
        $this->visivel = true;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return Passeio
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return Passeio
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Passeio
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     * @return Passeio
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return float
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param floar $preco
     * @return Passeio
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

    /**
     * @return float
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param float $desconto
     * @return Passeio
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;
        return $this;
    }

    /**
     * @return PacotePasseiosCategoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param PacotePasseiosCategoria $categoria
     * @return Passeio
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param ArrayCollection $imagens
     * @return Passeio
     */
    public function setImagens($imagens)
    {
        foreach ($imagens as $imagem) {

            if ($imagem->getPath() != null) {
                $imagem->setPasseio($this);
            }
        }

        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return Passeio
     */
    public function getPasseioReferencia()
    {
        return $this->passeioReferencia;
    }

    /**
     * @param Passeio $passeioReferencia
     * @return Passeio
     */
    public function setPasseioReferencia($passeioReferencia)
    {
        $this->passeioReferencia = $passeioReferencia;
        return $this;
    }

    /**
     * @return PacotePasseios
     */
    public function getPacotePasseios()
    {
        return $this->pacotePasseios;
    }

    /**
     * @param PacotePasseios $pacotePasseios
     * @return Passeio
     */
    public function setPacotePasseios($pacotePasseios)
    {
        $this->pacotePasseios = $pacotePasseios;
        return $this;
    }

    /**
     * @return User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param User $usuario
     * @return Passeio
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return Passeio
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getHorariosIndisponiveis()
    {
        return $this->horariosIndisponiveis;
    }

    /**
     * @param ArrayCollection $horariosIndisponiveis
     * @return Passeio
     */
    public function setHorariosIndisponiveis($horariosIndisponiveis)
    {
        $this->horariosIndisponiveis = $horariosIndisponiveis;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param boolean $ativo
     * @return Passeio
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getVisivel()
    {
        return $this->visivel;
    }

    /**
     * @param boolean $visivel
     * @return Passeio
     */
    public function setVisivel($visivel)
    {
        $this->visivel = $visivel;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getIntinerarios()
    {
        return $this->intinerarios;
    }

    /**
     * @param ArrayCollection $intinerarios
     * @return Passeio
     */
    public function setIntinerarios($intinerarios)
    {
        $this->intinerarios = $intinerarios;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPasseioPessoas()
    {
        return $this->passeioPessoas;
    }

    /**
     * @param ArrayCollection $passeioPessoas
     * @return Passeio
     */
    public function setPasseioPessoas($passeioPessoas)
    {
        $this->passeioPessoas = $passeioPessoas;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }
}
