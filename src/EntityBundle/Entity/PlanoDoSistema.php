<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plano_sistema")
 * @ORM\Entity()
 */
class PlanoDoSistema
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $nome;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $valor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $desconto;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $ativo;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PlanoDoSistema
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return PlanoDoSistema
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param float $valor
     * @return PlanoDoSistema
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return int
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param int $desconto
     * @return PlanoDoSistema
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param boolean $ativo
     * @return PlanoDoSistema
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

}
