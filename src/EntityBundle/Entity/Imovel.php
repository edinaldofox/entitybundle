<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="imovel")
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\ImovelRepository")
 */
class Imovel //implements ProdutoInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $titulo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $star;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descricao;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descricaoArredores;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dataCadastro;

    /**
     * @ORM\Column(type="float")
     */
    protected $preco;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $quantidade;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = false})
     */
    protected $isPromocao;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = false})
     */
    protected $isDestaque;


    protected $tipoDeProduto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $desconto;

    /**
     * @ORM\OneToOne(targetEntity="ImovelEndereco", mappedBy="imovel", cascade={"persist"})
     **/
    protected $endereco;

    /**
     * @ORM\ManyToOne(targetEntity="ImovelCategoria")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", unique = false, nullable=true)
     **/
    protected $categoria;

    /**
     * @var
     * @ORM\ManyToMany(targetEntity="EntityBundle\Entity\ImovelInstalacao")
     * @ORM\JoinTable(name="imovel_instalacoes",
     *      joinColumns={@ORM\JoinColumn(name="imovel_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="instalacao_id", referencedColumnName="id")}
     *      )
     */
    protected $instalacoes;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\ImovelImagens", mappedBy="imovel", cascade={"persist"})
     **/
    protected $imagens;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\User")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", unique = false)
     */
    protected $usuario;

    /**
     * @ORM\Column(type="string")
     **/
    protected $propriedade;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $condominio;

    /**
     * @ORM\Column(type="integer")
     */
    protected $numeroQuartos;

    /**
     * @ORM\Column(type="integer")
     */
    protected $numeroSuites;

    /**
     * @ORM\Column(type="integer")
     */
    protected $numeroBanheiro;

    /**
     * @ORM\Column(type="integer")
     */
    protected $numeroPessoas;

    /**
     * @ORM\ManyToMany(targetEntity="EntityBundle\Entity\ImovelGeral")
     * @ORM\JoinTable(name="imovel_geral",
     *      joinColumns={@ORM\JoinColumn(name="imovel_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="geral_id", referencedColumnName="id")}
     *      )
     */
    protected $geral;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $crianca;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $fumantes;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $festaEvento;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $deficiente;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $animais;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $longoPrazo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAtivo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isVisivel;

    public function __construct() {
        $this->imagens = new ArrayCollection();
        $this->instalacoes = new ArrayCollection();
        $this->geral = new ArrayCollection();
        $this->dataCadastro = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStar()
    {
        return $this->star;
    }

    /**
     * @param mixed $star
     */
    public function setStar($star)
    {
        $this->star = $star;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPromocao()
    {
        return $this->isPromocao;
    }

    /**
     * @param mixed $isPromocao
     */
    public function setIsPromocao($isPromocao)
    {
        $this->isPromocao = $isPromocao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param mixed $desconto
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     * @return Imovel
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstalacoes()
    {
        return $this->instalacoes;
    }

    /**
     * @param mixed $instalacoes
     */
    public function setInstalacoes($instalacoes)
    {
        $this->instalacoes = $instalacoes;
        return $this;
    }

    public function addInstalacoes(ImovelInstalacao $instalacao)
    {
        $this->instalacoes->add($instalacao);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagens()
    {
        return $this->imagens;
    }

    /**
     * @param $imagens
     * @return Imovel
     */
    public function setImagens($imagens)
    {

        foreach ($imagens as $imagem) {

            if ($imagem->getPath() != null) {
                $imagem->setImovel($this);
            }
        }

        $this->imagens = $imagens;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDestaque()
    {
        return $this->isDestaque;
    }

    /**
     * @param mixed $isDestaque
     */
    public function setIsDestaque($isDestaque)
    {
        $this->isDestaque = $isDestaque;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param mixed $isAtivo
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    public function addImagem($imagem)
    {
        $this->imagens->add($imagem);
        return $this;
    }

    /**
     * @return TipoProduto
     */
    public function getTipoDeProduto()
    {
        return $this->tipoDeProduto;
    }

    /**
     * @param mixed $tipoDeProduto
     * @return Imovel
     */
    public function setTipoDeProduto($tipoDeProduto)
    {
        $this->tipoDeProduto = $tipoDeProduto;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsVisivel()
    {
        return $this->isVisivel;
    }

    /**
     * @param mixed $isVisivel
     */
    public function setIsVisivel($isVisivel)
    {
        $this->isVisivel = $isVisivel;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param mixed $quantidade
     * @return Imovel
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param mixed $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescricaoArredores()
    {
        return $this->descricaoArredores;
    }

    /**
     * @param mixed $descricaoArredores
     */
    public function setDescricaoArredores($descricaoArredores)
    {
        $this->descricaoArredores = $descricaoArredores;
    }

    /**
     * @return mixed
     */
    public function getPropriedade()
    {
        return $this->propriedade;
    }

    /**
     * @param mixed $propriedade
     */
    public function setPropriedade($propriedade)
    {
        $this->propriedade = $propriedade;
    }

    /**
     * @return mixed
     */
    public function getCondominio()
    {
        return $this->condominio;
    }

    /**
     * @param mixed $condominio
     */
    public function setCondominio($condominio)
    {
        $this->condominio = $condominio;
    }

    /**
     * @return mixed
     */
    public function getNumeroQuartos()
    {
        return $this->numeroQuartos;
    }

    /**
     * @param mixed $numeroQuartos
     */
    public function setNumeroQuartos($numeroQuartos)
    {
        $this->numeroQuartos = $numeroQuartos;
    }

    /**
     * @return mixed
     */
    public function getNumeroSuites()
    {
        return $this->numeroSuites;
    }

    /**
     * @param mixed $numeroSuites
     */
    public function setNumeroSuites($numeroSuites)
    {
        $this->numeroSuites = $numeroSuites;
    }

    /**
     * @return mixed
     */
    public function getNumeroBanheiro()
    {
        return $this->numeroBanheiro;
    }

    /**
     * @param mixed $numeroBanheiro
     */
    public function setNumeroBanheiro($numeroBanheiro)
    {
        $this->numeroBanheiro = $numeroBanheiro;
    }

    /**
     * @return mixed
     */
    public function getNumeroPessoas()
    {
        return $this->numeroPessoas;
    }

    /**
     * @param mixed $numeroPessoas
     */
    public function setNumeroPessoas($numeroPessoas)
    {
        $this->numeroPessoas = $numeroPessoas;
    }

    /**
     * @return mixed
     */
    public function getGeral()
    {
        return $this->geral;
    }

    /**
     * @param mixed $geral
     */
    public function setGeral($geral)
    {
        $this->geral = $geral;
    }

    public function addGeral(ImovelGeral $geral)
    {
        $this->geral->add($geral);
    }

    /**
     * @return mixed
     */
    public function getCrianca()
    {
        return $this->crianca;
    }

    /**
     * @param mixed $crianca
     */
    public function setCrianca($crianca)
    {
        $this->crianca = $crianca;
    }

    /**
     * @return mixed
     */
    public function getFumantes()
    {
        return $this->fumantes;
    }

    /**
     * @param mixed $fumantes
     */
    public function setFumantes($fumantes)
    {
        $this->fumantes = $fumantes;
    }

    /**
     * @return mixed
     */
    public function getFestaEvento()
    {
        return $this->festaEvento;
    }

    /**
     * @param mixed $festaEvento
     */
    public function setFestaEvento($festaEvento)
    {
        $this->festaEvento = $festaEvento;
    }

    /**
     * @return mixed
     */
    public function getDeficiente()
    {
        return $this->deficiente;
    }

    /**
     * @param mixed $deficiente
     */
    public function setDeficiente($deficiente)
    {
        $this->deficiente = $deficiente;
    }

    /**
     * @return mixed
     */
    public function getAnimais()
    {
        return $this->animais;
    }

    /**
     * @param mixed $animais
     */
    public function setAnimais($animais)
    {
        $this->animais = $animais;
    }

    /**
     * @return mixed
     */
    public function getLongoPrazo()
    {
        return $this->longoPrazo;
    }

    /**
     * @param mixed $longoPrazo
     */
    public function setLongoPrazo($longoPrazo)
    {
        $this->longoPrazo = $longoPrazo;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

}
