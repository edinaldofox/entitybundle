<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Imagem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="passeios_imagens")
 */
class PasseioImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Passeio", inversedBy="imagens")
     * @ORM\JoinColumn(name="pacote_passeios_id", referencedColumnName="id")
     **/
    protected $passeio;

    /**
     * @return Passeio
     */
    public function getPasseio()
    {
        return $this->passeio;
    }

    /**
     * @param Passeio $passeio
     * @return PasseiosImagens
     */
    public function setPasseio($passeio)
    {
        $this->passeio = $passeio;
        return $this;
    }
}