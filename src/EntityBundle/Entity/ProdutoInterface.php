<?php

namespace EntityBundle\Entity;

interface ProdutoInterface
{

    public function getId();
    public function setId($id);

    public function getNome();
    public function setNome($nome);

    public function getDescricao();
    public function setDescricao($descricao);

    public function getPreco();
    public function setPreco($preco);

    public function getDesconto();
    public function setDesconto($desconto);

    public function getQuantidade();
    public function setQuantidade($quantidade);

    public function getImagens();
    public function setImagens($imagens);

    public function getTipoDeProduto();
    public function setTipoDeProduto($tipoDeProduto);

    public function getIsAtivo();
    public function setIsAtivo($isAtivo);

    public function getIsVisivel();
    public function setIsVisivel($isVisivel);
}