<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Imagem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="pacote_passeios_imagens")
 */
class PacotePasseiosImagens extends Imagem
{

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\PacotePasseios", inversedBy="imagens")
     * @ORM\JoinColumn(name="pacote_passeios_id", referencedColumnName="id")
     **/
    protected $pacotePasseios;

    /**
     * @return PacotePasseios
     */
    public function getPacotePasseios()
    {
        return $this->pacotePasseios;
    }

    /**
     * @param PacotePasseios $pacotePasseios
     * @return PacotePasseiosImagens
     */
    public function setPacotePasseios($pacotePasseios)
    {
        $this->pacotePasseios = $pacotePasseios;
        return $this;
    }
}