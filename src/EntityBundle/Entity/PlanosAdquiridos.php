<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="planos_adquiridos")
 * @ORM\Entity()
 */
class PlanosAdquiridos
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="PlanoDoSistema")
     * @ORM\JoinColumn(name="plano_sistema_id", referencedColumnName="id", unique = false)
     */
    protected $plano;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PlanosAdquiridos
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return PlanoDoSistema
     */
    public function getPlano()
    {
        return $this->plano;
    }

    /**
     * @param PlanoDoSistema $plano
     * @return PlanosAdquiridos
     */
    public function setPlano($plano)
    {
        $this->plano = $plano;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return PlanosAdquiridos
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

}
