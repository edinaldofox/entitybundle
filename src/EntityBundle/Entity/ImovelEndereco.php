<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="imovel_endereco")
 */
class ImovelEndereco
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $pais;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Uf")
     * @ORM\JoinColumn(name="uf_id", referencedColumnName="uf")
     */
    protected $estado;

    /**
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $cidade;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $bairro;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $cep;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $numero;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $rua;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $complemento;

    /**
     * @ORM\OneToOne(targetEntity="Imovel", inversedBy="endereco")
     * @ORM\JoinColumn(name="imovel_id", referencedColumnName="id")
     **/
    protected $imovel;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * @param mixed $pais
     */
    public function setPais($pais)
    {
        $this->pais = $pais;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getRua()
    {
        return $this->rua;
    }

    /**
     * @param mixed $rua
     */
    public function setRua($rua)
    {
        $this->rua = $rua;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
        return $this;
    }

    /**
     * @return Imovel
     */
    public function getImovel()
    {
        return $this->imovel;
    }

    /**
     * @param mixed $imovel
     * @return ImovelEndereco
     */
    public function setImovel($imovel)
    {
        $this->imovel = $imovel;
        return $this;
    }


}