<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="mensagem_sistema")
 * @ORM\Entity()
 */
class MensagemSistema
{
    const ATIVA = 1,
        INATIVA = 2;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $titulo;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $mensagem;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $status_notificacao;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $data;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MensagemSistema
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return MensagemSistema
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getMensagem()
    {
        return $this->mensagem;
    }

    /**
     * @param string $mensagem
     * @return MensagemSistema
     */
    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusNotificacao()
    {
        return $this->status_notificacao;
    }

    /**
     * @param int $status_notificacao
     * @return MensagemSistema
     */
    public function setStatusNotificacao($status_notificacao)
    {
        $this->status_notificacao = $status_notificacao;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return MensagemSistema
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param datetime $data
     * @return MensagemSistema
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    

}
