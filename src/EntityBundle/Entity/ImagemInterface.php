<?php

namespace EntityBundle\Entity;


interface ImagemInterface
{

    public function getNome();
    public function setNome($nome);
    public function getPath();
    public function setPath($path);
    public function isAtivo();
    public function getOrdem();
    public function setOrdem($ordem);

}