<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pedidos")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\PedidoRepository")
 */
class Pedido
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=90, nullable=false)
     */
    protected $nome_loja;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $valor_total;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", unique = false)
     */
    protected $usuario;

//    /**
//     * @var
//     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\User")
//     * @ORM\JoinColumn(name="usuario_cobranca_id", referencedColumnName="id", unique = false)
//     */
//    protected $usuarioCobranca;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PedidoStatus", mappedBy="pedido", cascade={"persist"})
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="EntityBundle\Entity\PedidoProdutos", mappedBy="pedido", cascade={"persist"})
     */
    protected $produtos;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $data_pedido;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dataRepasse;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\MeiosDePagamento")
     * @ORM\JoinColumn(name="meio_de_pagamento__id", referencedColumnName="id", unique = false)
     */
    protected $meioDePagamento;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\FormasDePagamento")
     * @ORM\JoinColumn(name="forma_de_pagamento__id", referencedColumnName="id", unique = false, nullable=true)
     */
    protected $formaDePagamento;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $codigoMeioPagamento;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $codigoDaTransacao;

    /**
     * Esta data se refere quando foi processado pelo meio de pagamento
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dataProcessoMeioPagamento;

    /**
     * @var string
     * @ORM\Column(type="string", length=120, nullable=false)
     */
    protected $voucher;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $voucherIsDadoBaixa;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $voucherDataBaixa;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $voucherIsTransferido;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $voucherDataTranferido;

    public function __construct()
    {
        $this->status = new ArrayCollection();
        $this->produtos = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Pedido
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomeLoja()
    {
        return $this->nome_loja;
    }

    /**
     * @param string $nome_loja
     * @return Pedido
     */
    public function setNomeLoja($nome_loja)
    {
        $this->nome_loja = $nome_loja;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorTotal()
    {
        return $this->valor_total;
    }

    /**
     * @param float $valor_total
     * @return Pedido
     */
    public function setValorTotal($valor_total)
    {
        $this->valor_total = $valor_total;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return Pedido
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param User $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * @return User
     */
    public function getUsuarioCobranca()
    {
        return $this->usuarioCobranca;
    }

    /**
     * @param User $usuarioCobranca
     * @return Pedido
     */
    public function setUsuarioCobranca($usuarioCobranca)
    {
        $this->usuarioCobranca = $usuarioCobranca;
        return $this;
    }

    /**
     * @return dateTime
     */
    public function getDataPedido()
    {
        return $this->data_pedido;
    }

    /**
     * @param dateTime $data_pedido
     * @return Pedido
     */
    public function setDataPedido($data_pedido)
    {
        $this->data_pedido = $data_pedido;
        return $this;
    }

    /**
     * @return date
     */
    public function getDataRepasse()
    {
        return $this->dataRepasse;
    }

    /**
     * @param date $dataRepasse
     * @return Pedido
     */
    public function setDataRepasse($dataRepasse)
    {
        $this->dataRepasse = $dataRepasse;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param ArrayCollection $status
     * @return Pedido
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProdutos()
    {
        return $this->produtos;
    }

    /**
     * @param ArrayCollection $produtos
     * @return Pedido
     */
    public function setProdutos($produtos)
    {
        foreach ($produtos as $produto) {
            if ($produto) {
                $produto->setPedido($this);
            }
        }
        $this->produtos = $produtos;
        return $this;
    }

    /**
     * @return MeiosDePagamento
     */
    public function getMeioDePagamento()
    {
        return $this->meioDePagamento;
    }

    /**
     * @param MeiosDePagamento $meioDePagamento
     * @return Pedido
     */
    public function setMeioDePagamento($meioDePagamento)
    {
        $this->meioDePagamento = $meioDePagamento;
        return $this;
    }

    /**
     * @return FormasDePagamento
     */
    public function getFormaDePagamento()
    {
        return $this->formaDePagamento;
    }

    /**
     * @param FormasDePagamento $formaDePagamento
     * @return Pedido
     */
    public function setFormaDePagamento($formaDePagamento)
    {
        $this->formaDePagamento = $formaDePagamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodigoMeioPagamento()
    {
        return $this->codigoMeioPagamento;
    }

    /**
     * @param string $codigoMeioPagamento
     * @return Pedido
     */
    public function setCodigoMeioPagamento($codigoMeioPagamento)
    {
        $this->codigoMeioPagamento = $codigoMeioPagamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodigoDaTransacao()
    {
        return $this->codigoDaTransacao;
    }

    /**
     * @param string $codigoDaTransacao
     * @return Pedido
     */
    public function setCodigoDaTransacao($codigoDaTransacao)
    {
        $this->codigoDaTransacao = $codigoDaTransacao;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDataProcessoMeioPagamento()
    {
        return $this->dataProcessoMeioPagamento;
    }

    /**
     * @param datetime $dataProcessoMeioPagamento
     * @return Pedido
     */
    public function setDataProcessoMeioPagamento($dataProcessoMeioPagamento)
    {
        $this->dataProcessoMeioPagamento = $dataProcessoMeioPagamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * @param string $voucher
     * @return Pedido
     */
    public function setVoucher($voucher)
    {
        $this->voucher = $voucher;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getVoucherIsDadoBaixa()
    {
        return $this->voucherIsDadoBaixa;
    }

    /**
     * @param boolean $voucherIsDadoBaixa
     * @return Pedido
     */
    public function setVoucherIsDadoBaixa($voucherIsDadoBaixa)
    {
        $this->voucherIsDadoBaixa = $voucherIsDadoBaixa;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getVoucherDataBaixa()
    {
        return $this->voucherDataBaixa;
    }

    /**
     * @param datetime $voucherDataBaixa
     * @return Pedido
     */
    public function setVoucherDataBaixa($voucherDataBaixa)
    {
        $this->voucherDataBaixa = $voucherDataBaixa;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isVoucherIsTransferido()
    {
        return $this->voucherIsTransferido;
    }

    /**
     * @param boolean $voucherIsTransferido
     * @return Pedido
     */
    public function setVoucherIsTransferido($voucherIsTransferido)
    {
        $this->voucherIsTransferido = $voucherIsTransferido;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getVoucherDataTranferido()
    {
        return $this->voucherDataTranferido;
    }

    /**
     * @param datetime $voucherDataTranferido
     * @return Pedido
     */
    public function setVoucherDataTranferido($voucherDataTranferido)
    {
        $this->voucherDataTranferido = $voucherDataTranferido;
        return $this;
    }

}
