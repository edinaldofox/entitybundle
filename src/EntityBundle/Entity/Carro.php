<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class Carro
{

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=225, unique=false, nullable=true)
     */
    protected $nome;

    /**
     * @var CarroMarca
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\CarroMarca")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id", unique = false, nullable=false)
     */
    protected $marca;

    /**
     * @var CarroModelo
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\CarroModelo")
     * @ORM\JoinColumn(name="modelo_id", referencedColumnName="id", unique = false, nullable=false)
     */
    protected $modelo;

    /**
     * @var CarroCambio
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\CarroCambio")
     * @ORM\JoinColumn(name="cambio_id", referencedColumnName="id", unique = false, nullable=false)
     */
    protected $cambio;

    /**
     * @var CarroDirecao
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\CarroDirecao")
     * @ORM\JoinColumn(name="direcao_id", referencedColumnName="id", unique = false, nullable=false)
     */
    protected $direcao;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\CarroCombustivel")
     * @ORM\JoinColumn(name="combustivel_id", referencedColumnName="id", unique = false, nullable=false)
     */
    protected $combustivel;

    /**
     * @var CarroCor
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\CarroCor")
     * @ORM\JoinColumn(name="cor_id", referencedColumnName="id", unique = false, nullable=false)
     */
    protected $cor;

    /**
     * @var string
     * @ORM\Column(type="string", length=25, unique=false, nullable=true)
     */
    protected $motor;

    /**
     * @var int
     * @ORM\Column(type="boolean")
     */
    protected $arCondicionado;

    /**
     * @var int
     * @ORM\Column(type="boolean")
     */
    protected $airBag;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $portas;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $passageiros;

    /**
     * @var bolean
     * @ORM\Column(type="boolean")
     */
    protected $vidroEletrico;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $freiosAbs;

    /**
     * Ex: AM/FM
     * @var string
     * @ORM\Column(type="string", length=225, unique=false, nullable=true)
     */
    protected $radio;

    /**
     * Ex:2 a 3
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $malas;

    /**
     * @var Empresa
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Carro
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Carro
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return CarroMarca
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * @param CarroMarca $marca
     * @return Carro
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
        return $this;
    }

    /**
     * @return CarroModelo
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * @param CarroModelo $modelo
     * @return Carro
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
        return $this;
    }

    /**
     * @return CarroCambio
     */
    public function getCambio()
    {
        return $this->cambio;
    }

    /**
     * @param CarroCambio $cambio
     * @return Carro
     */
    public function setCambio($cambio)
    {
        $this->cambio = $cambio;
        return $this;
    }

    /**
     * @return CarroDirecao
     */
    public function getDirecao()
    {
        return $this->direcao;
    }

    /**
     * @param CarroDirecao $direcao
     * @return Carro
     */
    public function setDirecao($direcao)
    {
        $this->direcao = $direcao;
        return $this;
    }

    /**
     * @return CarroCombustivel
     */
    public function getCombustivel()
    {
        return $this->combustivel;
    }

    /**
     * @param CarroCombustivel $combustivel
     * @return Carro
     */
    public function setCombustivel($combustivel)
    {
        $this->combustivel = $combustivel;
        return $this;
    }

    /**
     * @return CarroCor
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * @param CarroCor $cor
     * @return Carro
     */
    public function setCor($cor)
    {
        $this->cor = $cor;
        return $this;
    }

    /**
     * @return string
     */
    public function getMotor()
    {
        return $this->motor;
    }

    /**
     * @param string $motor
     * @return Carro
     */
    public function setMotor($motor)
    {
        $this->motor = $motor;
        return $this;
    }

    /**
     * @return int
     */
    public function getArCondicionado()
    {
        return $this->arCondicionado;
    }

    /**
     * @param int $arCondicionado
     * @return Carro
     */
    public function setArCondicionado($arCondicionado)
    {
        $this->arCondicionado = $arCondicionado;
        return $this;
    }

    /**
     * @return int
     */
    public function getAirBag()
    {
        return $this->airBag;
    }

    /**
     * @param int $airbag
     * @return Carro
     */
    public function setAirBag($airBag)
    {
        $this->airBag = $airBag;
        return $this;
    }

    /**
     * @return int
     */
    public function getPortas()
    {
        return $this->portas;
    }

    /**
     * @param int $portas
     * @return Carro
     */
    public function setPortas($portas)
    {
        $this->portas = $portas;
        return $this;
    }

    /**
     * @return integer
     */
    public function getPassageiros()
    {
        return $this->passageiros;
    }

    /**
     * @param integer $passageiros
     * @return Carro
     */
    public function setPassageiros($passageiros)
    {
        $this->passageiros = $passageiros;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getVidroEletrico()
    {
        return $this->vidroEletrico;
    }

    /**
     * @param boolean $vidroEletrico
     * @return Carro
     */
    public function setVidroEletrico($vidroEletrico)
    {
        $this->vidroEletrico = $vidroEletrico;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getFreiosAbs()
    {
        return $this->freiosAbs;
    }

    /**
     * @param boolean $freiosAbs
     * @return Carro
     */
    public function setFreiosAbs($freiosAbs)
    {
        $this->freiosAbs = $freiosAbs;
        return $this;
    }

    /**
     * @return string
     */
    public function getRadio()
    {
        return $this->radio;
    }

    /**
     * @param string $radio
     * @return Carro
     */
    public function setRadio($radio)
    {
        $this->radio = $radio;
        return $this;
    }

    /**
     * @return int
     */
    public function getMalas()
    {
        return $this->malas;
    }

    /**
     * @param integer $malas
     * @return Carro
     */
    public function setMalas($malas)
    {
        $this->malas = $malas;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return Carro
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }
}
