<?php

namespace EntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="locadora_rent_a_car_veiculo")
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="EntityBundle\Entity\Repository\LocadoraVeiculoRepository")
 */
class LocadoraVeiculo
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var LocadoraRentACar
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\LocadoraRentACar", inversedBy="locadoraVeiculo")
     * @ORM\JoinColumn(name="locadora_id", referencedColumnName="id")
     */
    private $locadora;

    /**
     * @var RentCar
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\RentCar")
     * @ORM\JoinColumn(name="veiculo_id", referencedColumnName="id")
     */
    private $veiculo;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return LocadoraRentACar
     */
    public function getLocadora()
    {
        return $this->locadora;
    }

    /**
     * @param LocadoraRentACar $locadora
     * @return LocadoraVeiculo
     */
    public function setLocadora($locadora)
    {
        $this->locadora = $locadora;
        return $this;
    }

    /**
     * @return RentCar
     */
    public function getVeiculo()
    {
        return $this->veiculo;
    }

    /**
     * @param RentCar $veiculo
     * @return LocadoraVeiculo
     */
    public function setVeiculo($veiculo)
    {
        $this->veiculo = $veiculo;
        return $this;
    }

}