<?php

namespace EntityBundle\Entity\Repository;

use EntityBundle\Entity\Pessoa;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityRepository;

class RentCarRepository extends EntityRepository
{

    protected $db;

    public function getCarrosListagemPorPessoa(Pessoa $pessoa, $inicio = 0, $total = 1)
    {

        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('EntityBundle\Entity\RentCar', 'c');
        $rsm->addFieldResult('c', 'id', 'id');
        $rsm->addFieldResult('c', 'preco', 'preco');

        $registro = $this->getEntityManager()
            ->createNativeQuery('
        SELECT id, preco FROM rent_car where pessoa_id = :pessoa order by preco ASC limit :inicio,:total',
                $rsm);

        $registro->setParameter('total', $total);
        $registro->setParameter('inicio', $inicio);
        $registro->setParameter('pessoa', $pessoa->getId());

        return $registro->getResult();


    }

}
