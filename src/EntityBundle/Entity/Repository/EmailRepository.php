<?php

namespace EntityBundle\Entity\Repository;

use EntityBundle\Entity\Empresa;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;

class EmailRepository extends EntityRepository
{

    public function totalMensagens(Empresa $empresa)
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->select('count(email.id)')
            ->from('SistemaBundle:Email','email')
            ->where('email.ativo = :ativo')
            ->andWhere('email.empresa = :empresa')
            ->setParameter('ativo', 1)
            ->setParameter('empresa', $empresa->getId());

        return $qb->getQuery()->getSingleScalarResult();

    }

    public function ultimasMensagens(Empresa $empresa)
    {

        $qb = $this->createQueryBuilder('e')
            ->where('e.ativo = :ativo')
            ->andWhere('e.empresa = :empresa')
            ->setParameter('ativo', 1)
            ->setParameter('empresa', $empresa->getId())
            ->orderBy('e.data', 'DESC')
            ->setMaxResults(3);

        return $qb->getQuery()->getResult();

    }

}
