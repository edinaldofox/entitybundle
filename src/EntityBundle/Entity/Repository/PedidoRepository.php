<?php

namespace EntityBundle\Entity\Repository;

use Carbon\Carbon;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\Pessoa;
use EntityBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Date;

class PedidoRepository extends EntityRepository
{

    public function totalPedidos(Empresa $empresa)
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->select('count(pedido.id)')
            ->from('SistemaBundle:Pedido','pedido')
            ->andWhere('pedido.empresa = :empresa')
            ->andWhere('pedido.data_pedido > :data')
            ->setParameter('data', new \DateTime(date("F d Y 00:00:00")))
            ->setParameter('empresa', $empresa->getId());

        return $qb->getQuery()->getSingleScalarResult();

    }

    public function pedidosPorMes(User $user, Carbon $carbon)
    {
        $dataInicial = Carbon::create($carbon->year, $carbon->month, 1);
        $dataFinal = Carbon::create($carbon->year, $carbon->month, $carbon->daysInMonth);

        $qb = $this->createQueryBuilder('pedido')
            ->andWhere('pedido.empresa = :empresa')
            ->andWhere('pedido.usuarioCobranca = :usuarioCobranca')
            ->andWhere('pedido.data_pedido BETWEEN :inicioMes AND :fimMes')
            ->setParameter('inicioMes', $dataInicial->format('Y-m-d'))
            ->setParameter('fimMes', $dataFinal->format('Y-m-d'))
            ->setParameter('empresa', $user->getEmpresa()->getId())
            ->setParameter('usuarioCobranca', $user->getId());

        return $qb->getQuery()->getResult();

    }

    public function pagamentosPorMes(User $user, Carbon $carbon)
    {
        $dataInicial = Carbon::create($carbon->year, $carbon->month, 1);
        $dataFinal = Carbon::create($carbon->year, $carbon->month, $carbon->daysInMonth);

        $qb = $this->createQueryBuilder('pedido')
            ->andWhere('pedido.empresa = :empresa')
            ->andWhere('pedido.usuarioCobranca = :usuarioCobranca')
            ->andWhere('pedido.dataRepasse BETWEEN :inicioMes AND :fimMes')
            ->setParameter('inicioMes', $dataInicial->format('Y-m-d'))
            ->setParameter('fimMes', $dataFinal->format('Y-m-d'))
            ->setParameter('empresa', $user->getEmpresa()->getId())
            ->setParameter('usuarioCobranca', $user->getId());

        return $qb->getQuery()->getResult();

    }

    public function getVoucher(Empresa $empresa)
    {

        $qb = $this->createQueryBuilder('pedido')
            ->andWhere('pedido.voucherIsDadoBaixa = :baixa')
            ->setParameter('baixa', true);

        return $qb->getQuery()->getResult();

    }

}
