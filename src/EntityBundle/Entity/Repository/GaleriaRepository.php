<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use EntityBundle\Entity\Empresa;

class GaleriaRepository extends EntityRepository
{

    /**
     * Pega as galerias ativas no sistema
     * @return array
     */
    public function getGalerias(Empresa $empresa)
    {

        $qb = $this->createQueryBuilder('g')
            ->where('g.isAtivo = :ativo')
            ->andWhere('g.empresa = :empresa')
            ->setParameter('ativo', true)
            ->setParameter('empresa', $empresa->getId())
            ->getQuery()
            ->getResult();

        return $qb;
    }

}

