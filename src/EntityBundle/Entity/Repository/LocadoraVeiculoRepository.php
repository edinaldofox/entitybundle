<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\Query\ResultSetMapping;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\Pessoa;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityRepository;

class LocadoraVeiculoRepository extends EntityRepository
{

    public function listaLocadoraVeiculo()
    {
        $qb = $this->createQueryBuilder('lv')
            ->groupBy('lv.locadora');

        return $qb->getQuery()->getResult();
    }

}
