<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use EntityBundle\Entity\ImovelPropriedade;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EntityBundle\Entity\Empresa;

class ImovelRepository extends EntityRepository
{

    protected $db;

    public function getDestaque()
    {

        $qb = $this->createQueryBuilder('imovel')
            ->where('imovel.isDestaque = :destaque')
            ->andWhere('imovel.isAtivo = :ativo')
            ->andWhere('imovel.isVisivel = :visivel')
            ->setParameter('destaque', true)
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setMaxResults(2)
            ->orderBy('imovel.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function getImovelIndex()
    {
        $qb = $this->createQueryBuilder('imovel')
            ->where('imovel.isDestaque = :destaque')
            ->andWhere('imovel.isAtivo = :ativo')
            ->andWhere('imovel.isVisivel = :visivel')
            ->setParameter('destaque', false)
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setMaxResults(10)
            ->orderBy('imovel.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

//    public function getImovelPropriedade(Empresa $empresa, ImovelPropriedade $propriedade, $estado = 'PB', $total = 10)
//    {
//        $estado = is_null($estado)?'PB':$estado;
//
//        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
//        $rsm->addEntityResult('EntityBundle\Entity\Imovel', 'imovel');
//        $rsm->addFieldResult('imovel', 'id', 'id');
//        $rsm->addFieldResult('imovel', 'nome', 'nome');
//        $rsm->addFieldResult('imovel', 'titulo', 'titulo');
//        $rsm->addFieldResult('imovel', 'endereco', 'endereco');
////        $rsm->addFieldResult('imovel', 'imagens', 'imagens');
//        $rsm->addFieldResult('imovel', 'geral', 'geral');
//        $rsm->addFieldResult('imovel', 'preco', 'preco');
//        $rsm->addJoinedEntityResult('EntityBundle\Entity\ImovelEndereco' , 'endereco', 'imovel', 'endereco');
//        $rsm->addFieldResult('endereco', 'endereco_id', 'id');
//        $rsm->addFieldResult('endereco', 'endereco_uf', 'uf_id');
//        $rsm->addJoinedEntityResult('EntityBundle\Entity\ImovelImagens' , 'imagens', 'imovel', 'imagens');
//        $rsm->addFieldResult('imagens', 'imagens_id', 'id');
//
//        $registro = $this->getEntityManager()
//        ->createNativeQuery("
//        SELECT * FROM imovel as imovel
//         join imovel_endereco as endereco ON endereco.imovel_id = imovel.id
//         join imovel_imagens as imagens ON imagens.imovel_id = imovel.id
//         where imovel.isVisivel = :isVisivel
//            AND imovel.isAtivo = :isAtivo
//            AND imovel.empresa_id = :empresa
//            AND imovel.propriedade = :propriedade
//            AND endereco.uf_id = :estado
//         order by imovel.id DESC limit :total",
//            $rsm);
//        $registro->setParameter('total', $total);
//        $registro->setParameter('empresa', $empresa->getId());
//        $registro->setParameter('isAtivo', true);
//        $registro->setParameter('isVisivel', true);
//        $registro->setParameter('propriedade', $propriedade->getNome());
//        $registro->setParameter('estado', $estado);
//
//        return $registro->getResult();
//    }

    public function getImovelPropriedade(Empresa $empresa, ImovelPropriedade $propriedade, $estado = 'PB', $total = 10)
    {

        $qb = $this->createQueryBuilder('imovel')
            ->join('imovel.empresa', 'empresa')
            ->join('imovel.endereco', 'endereco')
//            ->where('imovel.isDestaque = :destaque')
            ->where('imovel.isAtivo = :ativo')
            ->andWhere('imovel.isVisivel = :visivel')
            ->andWhere('empresa.id = :empresaId')
            ->andWhere('endereco.estado = :estado')
            ->andWhere('imovel.propriedade = :propriedade')
//            ->setParameter('destaque', false)
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setParameter('empresaId', $empresa->getId())
            ->setParameter('estado', $estado)
            ->setParameter('propriedade', $propriedade->getNome())
            ->setMaxResults($total)
            ->orderBy('imovel.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function listaDeImoveis(Empresa $empresa, $total = 10, $randamico = false)
    {

        if($randamico) {
            $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
            $rsm->addEntityResult('EntityBundle\Entity\Imovel', 'imovel');
            $rsm->addFieldResult('imovel', 'id', 'id');
            $rsm->addFieldResult('imovel', 'nome', 'nome');
            $rsm->addFieldResult('imovel', 'titulo', 'titulo');
            $rsm->addFieldResult('imovel', 'endereco', 'endereco');
            $rsm->addFieldResult('imovel', 'imagens', 'imagens');
            $rsm->addFieldResult('imovel', 'geral', 'geral');
            $rsm->addFieldResult('imovel', 'preco', 'preco');

            $registro = $this->getEntityManager()
                ->createNativeQuery('SELECT * FROM imovel where isVisivel = :isVisivel and isAtivo = :isAtivo and empresa_id = :empresa order by random() limit :total', $rsm);
            $registro->setParameter('total', $total);
            $registro->setParameter('isAtivo', true);
            $registro->setParameter('isVisivel', true);
            $registro->setParameter('empresa', $empresa->getId());

            return $registro->getResult();
        } else {
            $qb = $this->createQueryBuilder('imovel');

            $qb->andWhere('imovel.isAtivo = :ativo')
                ->join('imovel.empresa', 'empresa')
                ->andWhere('empresa.id = :empresaId')
                ->setParameter('ativo', true)
                ->setParameter('empresaId', $empresa->getId())
                ->setMaxResults($total);

            return $qb->getQuery()->getResult();
        }
    }

    public function getImovelAleatorio(Empresa $empresa)
    {
        $repository = $this->getEntityManager()
            ->getRepository('SistemaBundle:Imovel');

        return $repository->createQueryBuilder('imovel')
            ->join('imovel.empresa', 'empresa')
            ->andWhere('imovel.isAtivo = :ativo')
            ->andWhere('imovel.isVisivel = :visivel')
            ->andWhere('empresa.id = :empresaId')
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setParameter('empresaId', $empresa->getId())
            ->orderBy('imovel.id', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
        ;
    }

    public function getListaDeImoveis(Empresa $empresa, $estado, $total)
    {

        $repository = $this->getEntityManager()
            ->getRepository('SistemaBundle:Imovel');

        $this->db = $repository->createQueryBuilder('imovel')
            ->join('imovel.empresa', 'empresa')
            ->join('imovel.endereco', 'endereco')
            ->andWhere('imovel.isAtivo = :ativo')
            ->andWhere('imovel.isVisivel = :visivel')
            ->andWhere('empresa.id = :empresaId')
            ->andWhere('endereco.estado = :estado')
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setParameter('empresaId', $empresa->getId())
            ->setParameter('estado', $estado)
            ->setMaxResults($total)
            ;

        return $this;
    }

    public function imovelFiltro(Empresa $empresa, $filtro, $total= 10)
    {
        $qb = $this->createQueryBuilder('imovel')
            ->join('imovel.empresa', 'empresa')
            ->join('imovel.endereco', 'endereco')
            ->join('endereco.estado', 'uf');

        $qb->Where('imovel.isAtivo = :ativo')
            ->andWhere('imovel.isVisivel = :visivel')
            ->andWhere('empresa.id = :empresaId')
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setParameter('empresaId', $empresa->getId())
            ->setMaxResults($total)
            ->orderBy('imovel.id', 'DESC');

        if(!empty($filtro['nome'])) {

            $qb->andWhere('lower(imovel.nome) like lower(:nome)')
                ->setParameter('nome', '%'.$filtro['nome'].'%');
        }

        if(!empty($filtro['estado'])) {
            $qb->andWhere('uf.uf = :estado')
                ->setParameter('estado', $filtro['estado']);
        }

        if(!empty($filtro['cidade'])) {
            $qb->andWhere('cidade.id = :cidade')
                ->setParameter('cidade', $filtro['cidade']);
        }

        if(!empty($filtro['bairro'])) {
            $qb->andWhere('bairro.id = :bairro')
                ->setParameter('bairro', $filtro['bairro']);
        }

        if(!empty($filtro['descricao'])) {
            $qb->andWhere('imovel.propriedade = :propriedade')
                ->setParameter('propriedade', str_replace('-', ' ', $filtro['descricao']));
        }

        $this->db = $qb;

        return $this;
    }

    public function paginator($pagina, $totalPorPagina)
    {
        $this->db->setFirstResult($pagina)
            ->setMaxResults($totalPorPagina);

        return new Paginator($this->db);
    }

}