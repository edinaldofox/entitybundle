<?php

namespace EntityBundle\Entity\Repository;

use Carbon\Carbon;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\Pessoa;
use EntityBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Date;

class PacotePasseioRepository extends EntityRepository
{

    protected $db;

    public function getPacotes(Empresa $empresa, $filtro, $total = 10)
    {

        $this->db = $this->createQueryBuilder('papcotePasseio')
            ->join('papcotePasseio.empresa', 'empresa')
            ->join('papcotePasseio.passeios', 'passeios')
            ->Where('papcotePasseio.ativo = :ativo')
            ->andWhere('empresa.id = :empresaId')
            ->setParameter('ativo', true)
            ->setParameter('empresaId', $empresa->getId())
            ->setMaxResults($total)
            ->orderBy('papcotePasseio.id', 'DESC')
            ->orderBy('passeios.preco', 'ASC');

        return $this;

    }

    public function getPacotesPersonalizados(Empresa $empresa, $filtro, $estado, $inicio, $total = 10)
    {

        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('EntityBundle\Entity\PacotePasseios', 'pacotePasseios');
        $rsm->addFieldResult('pacotePasseios', 'id', 'id');
        $rsm->addFieldResult('pacotePasseios', 'nome', 'nome');
        $rsm->addFieldResult('pacotePasseios', 'titulo', 'titulo');
        $rsm->addFieldResult('pacotePasseios', 'descricao', 'descricao');
        $rsm->addFieldResult('pacotePasseios', 'imagens', 'imagens');
        $rsm->addJoinedEntityResult('EntityBundle\Entity\Uf' , 'estado', 'pacotePasseios', 'estado');
        $rsm->addFieldResult('estado', 'estado_id', 'estado');

        $registro = $this->getEntityManager()
            ->createNativeQuery('
        SELECT * FROM pacote_passeios as pacote
         join uf as estado ON estado.uf = pacote.uf_id
        where pacote.ativo = :isAtivo
            and pacote.empresa_id = :empresa
            and estado.uf = :estado
            order by pacote.id DESC limit :inicio,:total',
                $rsm);
        $registro->setParameter('total', $total);
        $registro->setParameter('inicio', $inicio);
        $registro->setParameter('empresa', $empresa->getId());
        $registro->setParameter('isAtivo', true);
        $registro->setParameter('estado', $estado);

        return $registro->getResult();

    }

    /**
     * Retorna o total de pacotes para a pagina de listagem e paginacao
     * @param Empresa $empresa
     * @return mixed
     */
    public function getTotalPacotesPersonalizados(Empresa $empresa, $estado)
    {


        $qb = $this->getEntityManager()->createQueryBuilder('pp');
        $qb->select(array('count(pp)'));
        $qb->from('SistemaBundle:PacotePasseios', 'pp')
            ->where('pp.estado = :estado')
            ->setParameter('estado', $estado)
        ;

        return $qb->getQuery()->getSingleScalarResult();

    }


    public function paginator($pagina, $totalPorPagina)
    {
        $this->db->setFirstResult($pagina)
            ->setMaxResults($totalPorPagina);

        return new Paginator($this->db);
    }

}
