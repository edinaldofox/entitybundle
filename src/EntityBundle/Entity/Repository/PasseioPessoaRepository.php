<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\Query\ResultSetMapping;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\Pessoa;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityRepository;

class PasseioPessoaRepository extends EntityRepository
{

    public function listaPasseioPessoa()
    {
        $qb = $this->createQueryBuilder('pp')
            ->groupBy('pp.pessoa');

        return $qb->getQuery()->getResult();
    }

    public function getMenorPreco(Pessoa $pessoa)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('passeio.id, passeio.nome, MIN(passeio.preco) as preco')
            ->from('SistemaBundle:PasseioPessoa','passeioPessoa')
            ->join('passeioPessoa.passeio', 'passeio')
            ->where('passeioPessoa.pessoa = :pessoa')
            ->setParameter('pessoa', $pessoa->getId());

        return $qb->getQuery()->getResult();
    }

    public function getMaiorPreco(Pessoa $pessoa)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('passeio.id, passeio.nome, MAX(passeio.preco) as preco')
            ->from('SistemaBundle:PasseioPessoa','passeioPessoa')
            ->join('passeioPessoa.passeio', 'passeio')
            ->where('passeioPessoa.pessoa = :pessoa')
            ->setParameter('pessoa', $pessoa->getId());

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function buscaPasseios(Empresa $empresa)
    {

//        $query = $this->_em->createQueryBuilder()
//            ->select('pass.id')
//            ->from('SistemaBundle:PasseioPessoa', 'pp')
//            ->join('pp.passeio','pass')
//            ->join('pp.pessoa','pess')
//            ->join('pp.empresa','emp')
//            ->where('pess.id = 2')
//            ->andWhere('emp.id = 2')
//            ->orderBy('pass.preco', 'ASC')
//            ->setMaxResults(2)
////            ->getQuery()->getSQL();
//            ->getDQL();
//dump($query);die;
//
//        SELECT * FROM `passeio_pessoa` as pp
//    join passeio as passeio ON passeio.id = pp.passeio_id
//    join pessoa as pessoa ON pessoa.id = pp.pessoa_id
//    where passeio.id = (SELECT pass.id FROM `passeio_pessoa` as passeio_p
//    join passeio as pass ON pass.id = passeio_p.passeio_id
//    join pessoa as pess ON pess.id = passeio_p.pessoa_id
//    where pess.id = pessoa.id
//    ORDER BY pass.preco ASC
//    limit 1)

//        SELECT passeio_pessoa.passeio_id AS passeio_id0, passeio_pessoa.pessoa_id AS pessoa_id1, passeio_pessoa.empresa_id AS empresa_id2
//        FROM passeio_pessoa passeio_pessoa
//        INNER JOIN passeio passeio ON passeio_pessoa.passeio_id = passeio.id
//        INNER JOIN pessoa pessoa ON passeio_pessoa.pessoa_id = pessoa.id
//        INNER JOIN empresa empresa ON passeio_pessoa.empresa_id = empresa.id
//        WHERE (passeio_.id = (
//        SELECT pass_.id
//                            FROM passeio_pessoa pp
//                            INNER JOIN passeio pass ON pp.passeio_id = pass.id
//                            INNER JOIN pessoa pess ON pp.pessoa_id = pess.id
//                            INNER JOIN empresa emp ON pp.empresa_id = emp.id
//                            WHERE pess.id = 2 AND emp.id = 2 ORDER BY pass.preco ASC limit 1
//                        )
//    ) AND empresa.id = 2

        
        
//        $qb =  $this->_em->createQueryBuilder();
//        $qb->select('passeioPessoa')
//            ->from('SistemaBundle:PasseioPessoa', 'passeioPessoa')
//            ->join('passeioPessoa.passeio', 'passeio')
//            ->join('passeioPessoa.pessoa', 'pessoa')
//            ->join('passeioPessoa.empresa', 'empresa')
//            ->where($qb->expr()->eq('passeio.id', "({$query})"))
////            ->where($qb->expr()->eq('pessoa.id', $query))
//            ->andWhere('empresa.id = :empresaId')
//            ->setParameter('empresaId', $empresa->getId())
//            ;
//
//        dump($qb->getQuery()->getSQL());die;
//        dump($qb->getQuery()->getResult());die;

        $query = "SELECT passeio_pessoa.id AS id, passeio_pessoa.passeio_id, passeio_pessoa.pessoa_id, passeio_pessoa.empresa_id
        FROM passeio_pessoa passeio_pessoa
        INNER JOIN passeio passeio ON passeio_pessoa.passeio_id = passeio.id
        INNER JOIN pessoa pessoa ON passeio_pessoa.pessoa_id = pessoa.id
        INNER JOIN empresa empresa ON passeio_pessoa.empresa_id = empresa.id
        WHERE (passeio.id = (
        SELECT pass.id
                            FROM passeio_pessoa pp
                            INNER JOIN passeio pass ON pp.passeio_id = pass.id
                            INNER JOIN pessoa pess ON pp.pessoa_id = pess.id
                            INNER JOIN empresa emp ON pp.empresa_id = emp.id
                            WHERE pess.id = pessoa.id AND emp.id = {$empresa->getId()} ORDER BY pass.preco ASC limit 1
                        )
    ) AND empresa.id = {$empresa->getId()}";


        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('EntityBundle\Entity\PasseioPessoa', 'passeioPessoa');
        $rsm->addFieldResult('passeioPessoa', 'id', 'id');
        $rsm->addFieldResult('passeioPessoa', 'passeio', 'passeio');
        $rsm->addFieldResult('passeioPessoa', 'pessoa', 'pessoa');
        $rsm->addFieldResult('passeioPessoa', 'empresa', 'emp   resa');
//        $rsm->addJoinedEntityResult('EntityBundle\Entity\Passeio', 'passeio', 'passeioPessoa', 'passeio');
//        $rsm->addFieldResult('passeio', 'id', 'id');
//        $rsm->addJoinedEntityResult('EntityBundle\Entity\Pessoa', 'pessoa', 'passeioPessoa', 'pessoa');
//        $rsm->addFieldResult('pessoa', 'id', 'id');
//        $rsm->addJoinedEntityResult('EntityBundle\Entity\Empresa', 'empresa', 'passeioPessoa', 'empresa');
//        $rsm->addFieldResult('empresa', 'id', 'id');
//        $rsm->addFieldResult('u', 'name', 'name'); // // ($alias, $columnName, $fieldName)

//        $query = $this->_em->createNativeQuery('call consultaPasseios(2)', $rsm);
        $query = $this->_em->createNativeQuery($query, $rsm);
//        $query->setParameter(1, '2');

        $users = $query->getResult();
        dump($users[0]);
dump($users[0]->getPasseio());die;
        return $qb->getQuery()->getResult();

    }

}
