<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\Query\ResultSetMapping;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\Pessoa;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityRepository;

class LocadoraRentACarRepository extends EntityRepository
{

    protected $db;

    public function getLocadoraPersonalizado(Empresa $empresa, $pagina, $total)
    {
        $this->db = $this->createQueryBuilder('l')
            ->join('l.empresa', 'empresa')
            ->where('empresa.id = :empresaId')
            ->setParameter('empresaId', $empresa->getId())
            ->setFirstResult($pagina)
            ->setMaxResults($total)
            ;

        return $this->db->getQuery()->getResult();
    }

    /**
     * Retorna o total de passeio para a pagina de listagem e paginacao
     * @param Empresa $empresa
     * @return mixed
     */
    public function getTotalLocadoraPersonalizados(Empresa $empresa)
    {


        $qb = $this->getEntityManager()->createQueryBuilder('l');
        $qb->select(array('count(l)'));
        $qb->from('SistemaBundle:LocadoraRentACar', 'l')
            ->join('l.empresa', 'empresa')
            ->where('empresa.id = :empresaId')
            ->setParameter('empresaId', $empresa->getId())
        ;

        return $qb->getQuery()->getSingleScalarResult();

    }

    public function paginator($pagina, $totalPorPagina)
    {
        $this->db->setFirstResult($pagina)
            ->setMaxResults($totalPorPagina);

        return new Paginator($this->db);
    }

}
