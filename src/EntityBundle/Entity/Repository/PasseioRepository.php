<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\PacotePasseios;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Date;

class PasseioRepository extends EntityRepository
{
    protected $db;

    public function getPedidos(Empresa $empresa, $filtro, $total = 10)
    {

        $this->db = $this->createQueryBuilder('passeio')
            ->join('passeio.empresa', 'empresa')
            ->Where('passeio.ativo = :ativo')
            ->andWhere('passeio.visivel = :visivel')
            ->andWhere('empresa.id = :empresaId')
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setParameter('empresaId', $empresa->getId())
            ->setMaxResults($total)
            ->orderBy('passeio.id', 'DESC');

        return $this;

    }

    public function getPasseioMenorValorPorPacote(PacotePasseios $pacotePasseios)
    {

        $this->db = $this->createQueryBuilder('passeio')
            ->join('passeio.pacotePasseios', 'pacotePasseios')
            ->Where('passeio.ativo = :ativo')
            ->andWhere('passeio.visivel = :visivel')
            ->andWhere('pacotePasseios = :pacotePasseio')
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setParameter('pacotePasseio', $pacotePasseios->getId())
            ->setMaxResults(1)
            ->orderBy('passeio.preco', 'ASC');

        return $this->db->getQuery()->getOneOrNullResult();

    }

    public function getPasseioPersonalizados(Empresa $empresa, $filtro, $estado, $inicio, $total = 10)
    {

        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('EntityBundle\Entity\Passeio', 'passeio');
        $rsm->addFieldResult('passeio', 'id', 'id');
        $rsm->addFieldResult('passeio', 'nome', 'nome');
        $rsm->addFieldResult('passeio', 'titulo', 'titulo');
        $rsm->addFieldResult('passeio', 'preco', 'preco');
        $rsm->addFieldResult('passeio', 'descricao', 'descricao');
        $rsm->addFieldResult('passeio', 'imagens', 'imagens');
        $rsm->addJoinedEntityResult('EntityBundle\Entity\Uf' , 'estado', 'passeio', 'estado');
        $rsm->addFieldResult('estado', 'estado_id', 'estado');

        $registro = $this->getEntityManager()
            ->createNativeQuery('
        SELECT * FROM passeio as passeio
        join uf as estado ON estado.uf = passeio.uf_id
        where passeio.ativo = :isAtivo
            and passeio.empresa_id = :empresa
            and estado.uf = :estado
            order by passeio.preco ASC limit :inicio,:total',
                $rsm);
        $registro->setParameter('total', $total);
        $registro->setParameter('inicio', $inicio);
        $registro->setParameter('empresa', $empresa->getId());
        $registro->setParameter('isAtivo', true);
        $registro->setParameter('estado', $estado);

        return $registro->getResult();

        return $this;

    }

    public function getPasseiosPorPacote(PacotePasseios $pacotePasseios, $filtro = null, $total = 10, $order = 'ASC')
    {

        $this->db = $this->createQueryBuilder('passeio')
            ->join('passeio.pacotePasseios', 'pacotePasseios')
            ->Where('passeio.ativo = :ativo')
            ->andWhere('passeio.visivel = :visivel')
            ->andWhere('pacotePasseios.id = :pacotePasseios')
            ->setParameter('ativo', true)
            ->setParameter('visivel', true)
            ->setParameter('pacotePasseios', $pacotePasseios->getId())
            ->setMaxResults($total)
            ->orderBy('passeio.preco', $order);

        return $this->db->getQuery()->getResult();

    }

    /**
     * Retorna o total de passeio para a pagina de listagem e paginacao
     * @param Empresa $empresa
     * @return mixed
     */
    public function getTotalPasseioPersonalizados(Empresa $empresa, $estado)
    {

        $qb = $this->getEntityManager()->createQueryBuilder('p');
        $qb->select(array('count(p.id)'));
        $qb->from('SistemaBundle:Passeio', 'p')
        ->where('p.estado = :estado')
        ->setParameter('estado', $estado)
        ;

        return $qb->getQuery()->getSingleScalarResult();

    }

    public function paginator($pagina, $totalPorPagina)
    {
        $this->db->setFirstResult($pagina)
            ->setMaxResults($totalPorPagina);

        return new Paginator($this->db);
    }

}
