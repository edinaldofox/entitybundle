<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\Query\ResultSetMapping;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\Pessoa;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityRepository;

class LocadoraPessoaRepository extends EntityRepository
{

    public function listaLocadoraPessoa()
    {
        $qb = $this->createQueryBuilder('lp')
            ->groupBy('lp.pessoa');

        return $qb->getQuery()->getResult();
    }

}
