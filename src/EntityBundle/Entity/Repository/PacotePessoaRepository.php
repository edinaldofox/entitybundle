<?php

namespace EntityBundle\Entity\Repository;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityRepository;

class PacotePessoaRepository extends EntityRepository
{

    public function listaPacotePessoa()
    {
        $qb = $this->createQueryBuilder('pp')
            ->groupBy('pp.pessoa');

        return $qb->getQuery()->getResult();
    }

}
