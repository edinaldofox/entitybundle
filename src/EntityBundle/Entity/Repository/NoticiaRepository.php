<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use BlogBundle\Entity\Categoria;
use Doctrine\ORM\Tools\Pagination\Paginator;

class NoticiaRepository extends EntityRepository
{

    protected $db;

    public function noticiasPorCategoria(Categoria $categoria)
    {

        $repository = $this->getEntityManager()
            ->getRepository('BlogBundle:Noticia');

        $this->db = $repository->createQueryBuilder('n')
            ->where('n.categoria = :categoria')
            ->setParameter('categoria', $categoria->getId())
            ->orderBy('n.dataCadastro', 'DESC');

        return $this;
    }

    public function noticias()
    {

        $repository = $this->getEntityManager()
            ->getRepository('BlogBundle:Noticia');

        $this->db = $repository->createQueryBuilder('n')
            ->orderBy('n.dataCadastro', 'DESC');

        return $this;
    }

    public function result()
    {
        return $this->db
            ->getQuery()
            ->getResult();
    }

    public function paginator($pagina, $totalPorPagina)
    {
        $this->db->setFirstResult($pagina)
            ->setMaxResults($totalPorPagina);

        return new Paginator($this->db);
    }
}