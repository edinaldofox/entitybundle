<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;
use EntityBundle\Entity\Empresa;
use EntityBundle\Entity\Pessoa;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityRepository;

class PessoaRepository extends EntityRepository
{

    protected $db;

    public function getPessoaPersonalizado(Empresa $empresa, $tipo, $filtro, $inicio, $total = 10)
    {

        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('EntityBundle\Entity\Pessoa', 'pessoa');
        $rsm->addFieldResult('pessoa', 'id', 'id');
        $rsm->addFieldResult('pessoa', 'nome', 'nome');
        $rsm->addFieldResult('pessoa', 'nomeFantasia', 'nomeFantasia');
        $rsm->addFieldResult('pessoa', 'descricao', 'descricao');

        $registro = $this->getEntityManager()
            ->createNativeQuery('
        SELECT id, nome, nomeFantasia, descricao FROM pessoa where empresa_id = :empresa and tipo_id = :tipo order by id DESC limit :inicio,:total',
                $rsm);
        $registro->setParameter('tipo', $tipo);
        $registro->setParameter('total', $total);
        $registro->setParameter('inicio', $inicio);
        $registro->setParameter('empresa', $empresa->getId());

        return $registro->getResult();

        return $this;

    }

    /**
     * Retorna o total de pacotes para a pagina de listagem e paginacao
     * @param Empresa $empresa
     * @return mixed
     */
    public function getTotalPessoas(Empresa $empresa, $tipo)
    {


        $qb = $this->getEntityManager()->createQueryBuilder('c');
        $qb->select(array('count(c)'));
        $qb->from('SistemaBundle:Pessoa', 'c')
        ->where('c.tipo = :tipo')
        ->setParameter('tipo', $tipo);

        return $qb->getQuery()->getSingleScalarResult();

    }


    public function paginator($pagina, $totalPorPagina)
    {
        $this->db->setFirstResult($pagina)
            ->setMaxResults($totalPorPagina);

        return new Paginator($this->db);
    }

}
