<?php

namespace EntityBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EntityBundle\Entity\ProdutoCategoria;
use EntityBundle\Entity\Empresa;

class ProdutoRepository extends EntityRepository
{
    protected $db;

    /**
     * @param Categoria $categoria
     * @return Paginator
     */
    public function produtosPorCategoria(Empresa $empresa, ProdutoCategoria $categoria)
    {

        $repository = $this->getEntityManager()
            ->getRepository('SistemaBundle:Produto');

        $this->db = $repository->createQueryBuilder('p')
            ->where('p.categoria = :categoria')
            ->andwhere('p.empresa = :empresa')
            ->setParameter('categoria', $categoria->getId())
            ->setParameter('empresa', $empresa->getId())
            ->orderBy('p.nome', 'ASC');

        return $this;

    }

    public function produtos(Empresa $empresa)
    {

        $repository = $this->getEntityManager()
            ->getRepository('SistemaBundle:Produto');

        $this->db = $repository->createQueryBuilder('p')
            ->andwhere('p.empresa = :empresa')
            ->setParameter('empresa', $empresa->getId())
            ->orderBy('p.nome', 'ASC');

        return $this;

    }

    public function produtosPorCategoriaQuantidade(Empresa $empresa, ProdutoCategoria $categoria, $quantidade)
    {

        $repository = $this->getEntityManager()
            ->getRepository('SistemaBundle:Produto');

        $query = $repository->createQueryBuilder('p')
            ->where('p.categoria = :categoria')
            ->andwhere('p.empresa = :empresa')
            ->setParameter('categoria', $categoria->getId())
            ->setParameter('empresa', $empresa->getId())
            ->orderBy('p.nome', 'ASC')
            ->setMaxResults($quantidade)
            ->getQuery();

        return $query->getResult();
    }

    public function result()
    {
        return $this->db
        ->getQuery()
        ->getResult();
    }

    public function paginator($pagina, $totalPorPagina)
    {
        $this->db->setFirstResult($pagina)
        ->setMaxResults($totalPorPagina);

        return new Paginator($this->db);
    }
}