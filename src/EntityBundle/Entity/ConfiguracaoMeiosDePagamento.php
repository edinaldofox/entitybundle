<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="configuracao_meios_pagamento")
 * @ORM\Entity()
 */
class ConfiguracaoMeiosDePagamento implements ConfiguracaoPagamentoInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $token;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $chave;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\Empresa", inversedBy="meiosDePagamento")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", unique = false)
     */
    protected $empresa;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $ativo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $transacaoTransparente;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\MeiosDePagamento")
     * @ORM\JoinColumn(name="meio_de_pagamento__id", referencedColumnName="id", unique = false)
     */
    protected $meioDePagamento;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param Empresa $empresa
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setEmpresa(Empresa $empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getChave()
    {
        return $this->chave;
    }

    /**
     * @param string $chave
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setChave($chave)
    {
        $this->chave = $chave;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param boolean $isAtivo
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTransacaoTransparente()
    {
        return $this->transacaoTransparente;
    }

    /**
     * @param boolean $transacaoTransparente
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setTransacaoTransparente($transacaoTransparente)
    {
        $this->transacaoTransparente = $transacaoTransparente;
        return $this;
    }

    /**
     * @return MeiosDePagamento
     */
    public function getMeioDePagamento()
    {
        return $this->meioDePagamento;
    }

    /**
     * @param MeiosDePagamento $meioDePagamento
     * @return ConfiguracaoMeiosDePagamento
     */
    public function setMeioDePagamento($meioDePagamento)
    {
        $this->meioDePagamento = $meioDePagamento;
        return $this;
    }

}
