<?php

namespace EntityBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\ImagemInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_imagem")
 */
class UserImagens implements ImagemInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $nome;

    /**
     * @ORM\ManyToOne(targetEntity="EntityBundle\Entity\User", inversedBy="imagens")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $path;

    /**
     * @ORM\Column(name="is_ativo", type="boolean", options={"default" = true})
     */
    protected $isAtivo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ordem;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param $nome
     * @return $this
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return Galeria
     */
    public function getGaleria()
    {
        return $this->galeria;
    }

    /**
     * @param $galeria
     * @return $this
     */
    public function setGaleria($galeria)
    {
        $this->galeria = $galeria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     * @return GaleriaImagens
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param mixed $isAtivo
     */
    public function isAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @return mixed
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * @param mixed $ordem
     * @return GaleriaImagens
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }
}